<!DOCTYPE html>
<html lang="pt-br">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="keywords" content="HTML5 Template" />
<meta name="description" content="Webster - Responsive Multi-purpose HTML5 Template" />
<meta name="author" content="potenzaglobalsolutions.com" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<title>Workshop Centauro</title>

<!-- Favicon -->
<link rel="shortcut icon" href="images/favicon.ico" />

<!-- font -->
<link  rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:200,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900">
<link href="https://fonts.googleapis.com/css?family=PT+Serif:400,400i,700,700i" rel="stylesheet">
 
<!-- Plugins -->
<link rel="stylesheet" type="text/css" href="template/css/plugins-css.css" />

<!-- Typography -->
<link rel="stylesheet" type="text/css" href="template/css/typography.css" />

<!-- Shortcodes -->
<link rel="stylesheet" type="text/css" href="template/css/shortcodes/shortcodes.css" />

<!-- Style -->
<link rel="stylesheet" type="text/css" href="template/css/style.css" />

<!-- Responsive -->
<link rel="stylesheet" type="text/css" href="template/css/responsive.css" /> 

<!-- Style customizer -->
<link rel="stylesheet" type="text/css" href="template/css/skins/skin-centauro.css" data-style="styles"/>

<!-- Style customizer -->
<link rel="stylesheet" type="text/css" href="assets/css/centauro.css" data-style="styles"/>

<style>
	.copyright{min-height: 55px; background: #cf3c48;
      	font-size: 1rem;
    }
    .links_footer{
      	list-style-type:none;
     	font-size: 1rem;
    }

    .bg-overlay-black-60:before {
    	background:rgba(21, 25, 27, 0.8);
    }

    .footer p{
    	font-size: 1rem !important;
    }

   .footer a:hover{
   		color:#cf3c48 !important;
   }

   .btnContrato{
		background-color: #cf3c48;
		transition: all 0.5s;
   }

   .btnContrato:hover{
   		background-color: transparent;
   		border-color: black;
   		color: black !important;
   		transition: all 0.5s;
   }

</style>

</head>

<body>
 
<div class="wrapper" id="home">

  <!--=================================
   preloader -->
    <div id="pre-loader">
      <img src="template/images/pre-loader/loader-13.svg" alt="">
    </div>
  <!--=================================
   preloader -->

	<!--=================================
	 header -->
		<header id="header" class="header light">
			<div class="menu">  
				<!-- menu start -->
				<nav id="menu" class="mega-menu">
					<!-- menu list items container -->
					<section class="menu-list-items">
						<div class="container"> 
							<div class="row"> 
								<div class="col-lg-12 col-md-12"> 
									<!-- menu logo -->
									<ul class="menu-logo">
										<li>
											<a href="./"><img id="logo_img"  class="img-fluid" src="assets/img/logo.png" alt=""> </a>
										</li>
									</ul>
									<!-- menu links -->
									<div class="menu-bar">
										<ul class="menu-links">
											<li><a href="./">Home</a></li>
											<li><a href="./#workshop">Sobre o Workshop</a></li>
											<li><a href="./#diretores">Convidados</a></li>
											<li><a href="./#centauro">A Centauro</a></li>
											<li><a href="./#duvidas">Dúvidas frequentes</a></li>
											<li><a class="inscrever" href="pagamento.php">Quero me inscrever</a></li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</section>
				</nav>
				<!-- menu end -->
			</div>
		</header>
	<!--=================================
	 header -->

	<!--=================================
	 page-title-->
		<section class="page-title bg-overlay-black-60 parallax" data-jarallax='{"speed": 0.6}' style="background-image: url(assets/img/bg/bg-5.jpg);">
			<div class="container">
				<div class="row"> 
					<div class="col-lg-12"> 
						<div class="page-title-name">
							<h1 class="m-0">Pagamento</h1>
						</div>
					</div>
				</div> 
			</div>
		</section>
	<!--=================================
	 page-title-->

	<!--=================================
	 page-container-->
		<section class="page-section-ptb">
			<div class="container">
				<div class="row d-flex align-items-center">
					<div class="col-md-7">
						<div class="row pricing-top m-0 px-2">
							<div class="col-12 mb-4">
								<div class="section-title line left text-center text-sm-left mb-3">
									<div class="title">
										<h5 class="mb-0">Sobre o Workshop<span class="d-none d-md-inline">:</span></h5>
									</div>
								</div>
								<div>
									Neste workshop, vamos abordar todo o processo de dublagem, desde técnicas, interpretação, sync, voice over, leitura dinâmica e adaptação textual à legislação, como funciona o mercado de trabalho, o acordo coletivo e, é claro, teremos muita, mas muita prática!								
								</div>
							</div>
							<div class="col-12 mb-4">
								<div class="section-title line left text-center text-sm-left mb-3">
									<div class="title">
										<h5 class="mb-0">Pré-requisitos<span class="d-none d-md-inline">:</span></h5>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12">
										<ul class="list list-unstyled list-hand">
											<li>Possuir DRT</li>
											<li>Ter 16 anos ou mais</li>
										</ul>
									</div>
									<!--<div class="col-sm-6">
										<ul class="list list-unstyled list-hand">
											<li>Lorem ipsum dolor</li>
											<li>Excepteur sint occaecat</li>
										</ul>
									</div>!-->
								</div>
							</div>
							<div class="col-12">
								<div class="row">
									<div class="col-lg-6 mb-4 mb-lg-0">
										<div class="section-title line left text-center text-sm-left mb-3">
											<div class="title">
												<h5 class="mb-0">Datas<span class="d-none d-md-inline">:</span></h5>
											</div>
										</div>
										<div class="box">
											<select class="fancyselect" tabindex="0">
												<option value="0" class="option">Data disponiveis<span class="d-none d-md-inline">:</span></option>
												<option value="1" class="option selected focus">03/08/2019</option>
												<!--<option value="2" class="option disabled" disabled>02/00/2018</option>
												<option value="3" class="option">03/00/2018</option>-->
											</select>
										</div>
									</div>
									<div class="col-lg-6">
										<div class="section-title line left text-center text-sm-left mb-3">
											<div class="title">
												<h5 class="mb-0">Local<span class="d-none d-md-inline">:</span></h5>
											</div>
										</div>
										<div class="">
											Av. Francisco Matarazzo, N° 999 - 10º Andar Barra Funda <br>
											CEP 05001-100 São Paulo, Brasil
										</div>
									</div>
								</div>
							</div>
							<div class="col-12 mb-4">
								<div class="section-title line left text-center text-sm-left mb-3 ">
									<div class="title">
										<h5 class="mb-0">Formas de Pagamento<span class="d-none d-md-inline">:</span></h5>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12">
										<img class="full-width" src="//assets.pagseguro.com.br/ps-integration-assets/banners/pagamento/avista_estatico_550_70.gif" alt="Logotipos de meios de pagamento do PagSeguro" title="Este site aceita pagamentos com as principais bandeiras e bancos, saldo em conta PagSeguro e boleto.">
									</div>
									<!--<div class="col-sm-6">
										<ul class="list list-unstyled list-hand">
											<li>Lorem ipsum dolor</li>
											<li>Excepteur sint occaecat</li>
										</ul>
									</div>!-->
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-5">
						<div class="contact-form clearfix">
							<form id="pagamento" action="solicita_pagamento.php" method="post">
							<!-- <form id="pagamento" name="pagamento" method="post" action="solicita_pagamento.php"> -->
								<div class="row">
									<div class="col-12 py-3 text-center">
										<h4>CADASTRO</h4>
									</div>
									<div class="col-12">
										<div class="section-field w-100">
											<input type="text" placeholder="Nome*" class="form-control"  name="nome" required="required">
										</div>
									</div>
									<div class="col-12">
										<div class="section-field w-100">
											<input type="text" placeholder="Telefone" class="form-control" id="telefone" name="telefone">
										</div>
									</div>
									<div class="col-12">
										<div class="section-field w-100">
											<input type="text" placeholder="Celular*" class="form-control" id="celular" name="celular" required="required">
										</div>
									</div>
									<div class="col-12">
										<div class="section-field w-100">
											<input type="email" placeholder="Email*" class="form-control" name="email" required="required">
										</div>
									</div>
									<div class="col-12">
										<div class="section-field w-100">
											<input type="text" id="cpf" placeholder="CPF*" class="form-control" name="cpf" required="required">
										</div>
									</div>
									<div class="col-12">
										<div class="section-field w-100">
											<input type="text" id="drt" placeholder="DRT*" class="form-control" name="drt" required="required">
										</div>
									</div>
									<div class="col-12">
										<div class="section-field w-100 ml-1">
											<input type="checkbox" id="politicas" name="politica" required="required">
  											<label for="politicas">Eu li, e aceito os termos da política de uso. <a href="politicas.php" target="_blank" data-toggle="modal" data-target="#contratoModal">Acessar contrato.</a></label>
										</div>
									</div>
								</div>
								<?php
									require_once "verificavagas.php";
								?>
								<!-- <div class="section-field textarea clearfix text-center pt-4">
									<button class="button dark border w-100 px-2">
										<span>EFETUAR PAGAMENTO</span> <i class="fa fa-money"></i>
									</button>
									<button class="button dark border w-100 m-0">
										<span>FAZER RESERVA</span> <i class="fa fa-calendar-check-o"></i>
									</button>
								</div> -->
							</form>
						</div>
					</div>
					<div class="col-12">
						<div class="section-field w-100 clearfix mt-4">
							<div class="alert alert-primary">
								É obrigatório ter a DRT para comprar o curso.
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- MODAL CONTRATO -->
		<div class="modal fade" id="contratoModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  			<div class="modal-dialog modal-lg" role="document">
    			<div class="modal-content">
			      <div class="modal-header d-flex justify-content-center text-center">
			        <h4 class="modal-title" id="ModalLabel">Contrato eletrônico de prestação de serviços de treinamento</h4>
			      </div>
			      <div class="modal-body">
			        <span>
			        	<p>Pelo presente Instrumento Particular de Prestação de Serviços, de um lado a empresa <b>CENTAURO COMUNICACIONES DO BRASIL EIRELI</b>, com sede Av. Francisco Matarazzo nº 999, 10º andar, Barra Funda, São Paulo, Capital, inscrita no CNPJ sob o n° 04.459.41/0001-39, neste ato representada por seu representante legal, doravante simplesmente denominada <b>CONTRATADA</b>, e de outro lado o <b>ALUNO</b>, qualificado no formulário eletrônico de matrícula, em que declara estar ciente e ter aceitado os termos do presente contrato, têm entre si, justo e pactuado o que segue:</p>

						<p><b>CLÁUSULA PRIMEIRA: DO OBJETO</b></p>

						<p>Constitui objeto do presente contrato a prestação de serviços de treinamento em processo de dublagem (“Workshop”), por parte da CONTRATADA, mediante a contraprestação por parte do ALUNO, determinada na Cláusula Quinta.</p>

						<p>Parágrafo Primeiro: A CONTRATADA se reserva ao direito de adiar o Workshop se não houver um número mínimo de 50% (cinquenta por cento) de alunos matriculados em relação ao número de vagas abertas. Neste caso, a CONTRATADA comunicará o adiamento do Workshop ao ALUNO, com até uma semana de antecedência da data descrita no Parágrafo Segundo desta Cláusula. Caso o ALUNO não possa comparecer na nova data estabelecida pela CONTRATADA, aplicar-se-ão as regras contidas na cláusula sétima deste contrato.</p>

						<p>Parágrafo Segundo: O Workshop será realizado na Avenida Francisco Matarazzo, nº 999, 10º andar, São Paulo, SP, nos dias 03/08 e 04/08/2019, das 09h às 16 horas (com uma hora de intervalo). O Workshop poderá ser remanejado para outro dia, a definir, por eventos de força maior (incluindo, mas não se limitando, à falha no fornecimento de energia elétrica, fortes chuvas, etc.).</p>

						<p>Parágrafo Terceiro: O Workshop é pessoal e intransferível, sendo que cada 01 (uma) vaga contratada refere-se a 01 (um) participante. Aqueles que não contrataram o Workshop pelo website da CONTRATADA não poderão, sob qualquer hipótese, participar do Workshop.</p>

						<p>Parágrafo Quarto: O conteúdo completo do Workshop e a relação dos profissionais ministrantes do curso estão disponíveis no website do Workshop.</p>

						<p>Parágrafo Quinto: <b>Somente poderá participar do Workshop o ALUNO que apresentar, no primeiro dia da realização do Workshop, registro profissional (DRT) emitido em seu nome. O ALUNO que não possuir ou não apresentar a DRT conforme especificado nesta cláusula, não poderá participar do Workshop e não terá qualquer valor devolvido</b>.</p>

						<p>Parágrafo Sexto: Não poderá participar do Wokshop e não terá qualquer valor devolvido o ALUNO maior de 16 e menor de 18 anos que não apresentar uma autorização escrita de seu responsável legal. Será proibida a participação de menores de 16 anos.</p>

						<p><b>CLÁUSULA SEGUNDA: DAS OBRIGAÇÕES DA CONTRATADA</b></p>

						<p>Além das demais obrigações previstas neste instrumento, constituem obrigações da CONTRATADA:</p>

						<p>a) Realizar e ministrar o Workshop para o ALUNO por meio de pessoal devidamente capacitado;</p>
						
						<p>b) Fornecer ao ALUNO material para a realização do Workshop;</p>

						<p>c) Fornecer certificado de participação ao ALUNO que tiver 100% (cem por cento) de frequência no Workshop.</p>

						<p>Parágrafo Único: A CONTRATADA não será obrigada a fornecer qualquer tipo de brinde, exceto aqueles eventualmente oferecidos no site de venda.</p>

						<p><b>CLÁUSULA TERCEIRA: DAS OBRIGAÇÕES DO ALUNO</b></p>

						<p>Além das demais obrigações previstas neste instrumento, constituem obrigações do ALUNO:</p>

						<p>a) Realizar o pagamento da retribuição pecuniária do serviço prestado pela CONTRATADA, na forma das disposições da Cláusula Quinta;</p>
						
						<p>b) Zelar e se responsabilizar, de forma individual e exclusiva, pelos seus bens pessoais antes, durante ou no trajeto para o Workshop;</p>
						
						<p>c) Apresentar DRT emitida em seu nome, quando do primeiro dia da realização do Workshop;</p>
						
						<p>d) Manter o mais completo e absoluto sigilo sobre o conteúdo do Workshop e quaisquer dados, materiais, informações, documentos, especificações técnicas e comerciais, inovações ou aperfeiçoamentos ou quaisquer outras informações, documentos e dados, sejam eles verbais ou escritos, a que tiver acesso, sem exceção, não podendo, sob qualquer pretexto, divulgá-los, revelá-los, ou deles dar conhecimento a terceiros, estranhos a esta contratação, sob pena de arcar com as perdas e danos cabíveis. Referida obrigação vigorará durante o prazo de vigência do Contrato e permanecerá vigente pelo prazo de 5 (cinco) anos após seu término.</p>

						<p><b>CLÁUSULA QUARTA: DO USO DE IMAGEM</b></p>
						
						<p>O ALUNO autoriza a CONTRATADA a utilizar sua imagem em todo e qualquer material entre imagens de vídeo, fotos e voz, capturados no Workshop. A presente autorização abrange o uso da imagem em todo território nacional e no exterior, das seguintes formas: (I) out-door; (II) busdoor; (III) folhetos em geral (encartes, mala direta, catálogo, etc.); (IV) folder de apresentação; (V) anúncios em revistas e jornais em geral; (VI) homepage; (VII) cartazes; (VIII) back-light; (IX) mídia eletrônica (painéis, vídeo-tapes, televisão, cinema, programa para rádio, entre outros).</p>

						<p>Parágrafo Primeiro: O ALUNO autoriza, ainda, a CONTRATADA a realizar, nas imagens e sons captados, cortes, reduções e edições. Esta autorização não gera e não gerará no futuro e também não ensejará interpretação de existir quaisquer vínculos ou obrigações trabalhistas, securitárias, previdenciária, indenizatória, ou mesmo empregatícia, entre o ALUNO e a CONTRATADA.</p>

						<p>Parágrafo Segundo: O ALUNO <b>DECLARA</b>, portanto, que está de acordo com o uso dessas imagens, que o seu uso não viola os direitos de imagem e de privacidade do ALUNO, e que tem ciência de que este material constituído por imagens e sons pertence exclusivamente à CONTRATADA, que poderá usá-lo a seu exclusivo critério.</p>

						<p>Parágrafo Terceiro: A presente licença de uso de imagem, nos termos aqui definidos, se dá a título gratuito e por prazo indeterminado, a contar da data de sua assinatura deste instrumento.</p>

						<p><b>CLÁUSULA QUINTA: DO VALOR E DA FORMA DE PAGAMENTO</b></p>

						<p>O ALUNO pagará à CONTRATADA o valor de R$ 900,00 (novecentos reais), com todos os tributos já inclusos, a serem pagos à vista ou em 03 (três) parcelas iguais de R$ 300,00 (trezentos reais) cada, via PagSeguro, diretamente no website do Workshop.</p>

						<p>Parágrafo Primeiro: No valor descrito no caput desta cláusula está incluso o material (apostila) relativo ao Workshop. A CONTRATADA não custeia ou reembolsa qualquer despesa havida pelo ALUNO para participar do Workshop, incluindo, mas não se limitando, passagens, hospedagem, alimentação, locomoção, despesas médicas, entre outras.</p>

						<p>Parágrafo Segundo: Se qualquer uma das parcelas não for quitada pelo ALUNO, através contraordem de pagamento à administradora do cartão de crédito, seja por qualquer outro motivo, a CONTRATADA poderá:</p>

						<p>a) Inscrever o ALUNO em cadastro ou serviços de proteção ao crédito, após a regular notificação prevista nas legislações civil e consumerista, e/ou;</p>

						<p>b) Promover a execução judicial da dívida ou qualquer outro meio, judicial ou extrajudicial, para recebimento do débito.</p>

						<p><b>CLÁUSULA SEXTA: DA DESISTÊNCIA DO WORKSHOP PELO ALUNO</b></p>

						<p>Caso o ALUNO desista de participar do Workshop, a CONTRATADA deverá devolver 50% (cinquenta por cento) do valor do Workshop caso a comunicação, por parte do ALUNO, ocorra com até 15 (quinze) dias de antecedência da data do Workshop.</p>

						<p>Parágrafo Primeiro: Caso o ALUNO comunique à CONTRATADA sobre sua desistência após o prazo previsto no caput desta cláusula, e até 07 dias antes da realização do Workshop, o ALUNO receberá de volta o montante de 25% (vinte e cinco por cento) do valor do Workshop.</p>

						<p>Parágrafo Segundo: Não haverá devolução de valores por motivo de falta, desistência, doenças e outros assuntos não informados com a antecedência estabelecida nesta cláusula.</p>

						<p><b>CLÁUSULA SÉTIMA: DO CANCELAMENTO DO WORKSHOP PELA CONTRATADA</b></p>

						<p>Caso a CONTRATADA cancele o Workshop, por qualquer motivo, deverá devolver integralmente todo e qualquer valor eventualmente pago pelo ALUNO em um prazo de 72 (setenta e duas) horas contado da comunicação do cancelamento.<br></p>

						<p><b>CLÁUSULA OITAVA: DA VIGÊNCIA</b>

						<p>Este contrato entra em vigência na data do seu aceite e se encerrará quando da finalização do Workshop.</p>

						<p><b>CLÁUSULA NONA: DA NATUREZA DO WORKSHOP</b><br></p>

						<p>O ALUNO expressamente declara estar ciente de que o Workshop não é um curso profissionalizante, não significa uma forma de certificação, sendo o certificado a ser emitido pela CONTRATADA apenas uma comprovação de participação no Workshop.</p>

						<p><b>CLÁUSULA DÉCIMA: DAS DISPOSIÇÕES GERAIS</b></p>

						<p>Ao clicar na opção ACEITAR, disponível ao ALUNO no website do Workshop, esse se declara ciente e de acordo com todos os termos do presente instrumento.</p>

						<p>Parágrafo Primeiro:  A comunicação entre as partes se dará através dos respectivos endereços eletrônicos das partes, razão pela qual deverá o ALUNO, sempre manter seus dados atualizados. O e-mail da CONTRATADA para receber comunicados dos ALUNOS é: cursos@centauro.com</p>

						<p>Parágrafo Segundo: Caso alguma das cláusulas destes termos de uso do site seja inválida ou declarada inválida por um tribunal, tal invalidação não afetará a validade das demais cláusulas.</p>

						<p>Parágrafo Terceiro: Este acordo não concede direitos a quaisquer terceiros.</p>

						<p>Parágrafo Quarto: O ALUNO não poderá utilizar o nome e/ou a marca da CONTRATADA sob qualquer hipótese, especialmente para a divulgação dos seus serviços, sob pena de incorrer as penas cíveis e criminais previstas em lei.</p>

						<p>Parágrafo Quinto: Em caso de conflito, o foro eleito pelas partes será o da Comarca de São Paulo, Capital, ou o do domicílio do ALUNO, caso seja de sua escolha.</p>

						<p>E, por ter lido e estar de acordo com o conteúdo do presente instrumento, o ALUNO opta por aceitá-lo em sua integralidade, fazendo com que surtam os efeitos jurídicos próprios.</p>
			        </span>
			      </div>
			      <div class="modal-footer">
			        <button type="button" class="btn btnContrato w-100 text-white" data-dismiss="modal">Fechar</button>
			      </div>
			   	</div>
			</div>
		</div>
	<!--=================================
	 page-container-->

<?php
	include 'footer.php';
?>
<script type="text/javascript" src="template/js/jquery-nice-select/jquery-nice-select.js"></script>

<!-- <script type="text/javascript" src="https://stc.pagseguro.uol.com.br/pagseguro/api/v2/checkout/pagseguro.lightbox.js"></script>
<script>
	function enviaPagseguro(){
		$.post('solicita_pagamento.php','',function(data)
		{
			alert('teste');
			$('#code').val(data);
			$('#pagamento').submit();
		})
	}
</script> -->