<?php 
	
	require_once "PagSeguro.php";


	if(isset($_GET['id_pagseguro']))
	{
		//print_r($_GET['id_pagseguro']);

		$pagseguro = new PagSeguro();
		$pagseguro->consultaTransacao($_GET['id_pagseguro']);

		session_start();

		if($_SESSION['tipo'] == 'reserva')
		{
			unset($_SESSION['tipo']);
			header("location: reserva.php");
		}
		else
		{
			unset($_SESSION['tipo']);
		}
	}
 ?>


<!DOCTYPE html>
<html lang="pt-br">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="keywords" content="HTML5 Template" />
<meta name="description" content="Webster - Responsive Multi-purpose HTML5 Template" />
<meta name="author" content="potenzaglobalsolutions.com" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<title>Webster - Responsive Multi-purpose HTML5 Template</title>

<!-- Favicon -->
<link rel="shortcut icon" href="images/favicon.ico" />

<!-- font -->
<link  rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:200,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900">
<link href="https://fonts.googleapis.com/css?family=PT+Serif:400,400i,700,700i" rel="stylesheet">
 
<!-- Plugins -->
<link rel="stylesheet" type="text/css" href="template/css/plugins-css.css" />

<!-- Typography -->
<link rel="stylesheet" type="text/css" href="template/css/typography.css" />

<!-- Shortcodes -->
<link rel="stylesheet" type="text/css" href="template/css/shortcodes/shortcodes.css" />

<!-- Style -->
<link rel="stylesheet" type="text/css" href="template/css/style.css" />

<!-- Responsive -->
<link rel="stylesheet" type="text/css" href="template/css/responsive.css" /> 

<!-- Style customizer -->
<link rel="stylesheet" type="text/css" href="template/css/skins/skin-centauro.css" data-style="styles"/>

<!-- Style customizer -->
<link rel="stylesheet" type="text/css" href="assets/css/centauro.css" data-style="styles"/>
 
</head>

<body>
 
<div class="wrapper" id="home">

  <!--=================================
   preloader -->
    <div id="pre-loader">
      <img src="template/images/pre-loader/loader-13.svg" alt="">
    </div>
  <!--=================================
   preloader -->

  <!--=================================
   header -->
   	<header id="header" class="header light">
		<div class="menu" >  
			<!-- menu start -->
			<nav id="menu" class="mega-menu">
				<!-- menu list items container -->
				<section class="menu-list-items">
					<div class="container"> 
						<div class="row"> 
							<div class="col-lg-12 col-md-12"> 
								<!-- menu logo -->
								<ul class="menu-logo">
									<li>
										<a href="#home"><img id="logo_img"  class="img-fluid" src="assets/img/logo.png" alt=""> </a>
									</li>
								</ul>
								<!-- menu links -->
								<div class="menu-bar">
									<ul class="menu-links">
										<li class="active"><a href="./">Home</a></li>
										<li><a href="./#workshop">Sobre Workshop</a></li>
										<li><a href="./#diretores">Diretores</a></li>
										<li><a href="./#centauro">A Centauro</a></li>
										<li><a href="./#duvidas">Dúvidas frequentes</a></li>
										<li><a class="inscrever" href="pagamento.php">Quero me inscrever</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</section>
			</nav>
			<!-- menu end -->
		</div>
	</header>
  <!--=================================
   header -->

	<!--=================================
	page-title-->
	<section class="page-title bg-overlay-black-60 parallax" data-jarallax='{"speed": 0.6}' style="background-image: url(assets/img/bg/bg-5.jpg);">
		<div class="container">
			<div class="row"> 
				<div class="col-lg-12"> 
					<div class="page-title-name">
						<h1 class="m-0">Pagamento</h1>
					</div>
				</div>
			</div> 
		</div>
	</section>
	<!--=================================
	page-title-->

	<!--=================================
	page-container-->
		<section class="page-section-ptb">
			<div class="container">
				<div class="row text-center">
					<div class="col-12">
						<i class="fa  fa-thumbs-o-up ft-35 fc-1"></i>
					</div>
					<div class="col-12 ft-25">
						<h3>Obrigado por se cadastrar no curso!</h3>
						<p>
							Assim que o pagamento for efetuado você receberá um email com a confirmação de matricula.
						</p>
					</div>
					<div class="col-12 mt-4">
						<div class="row">
							<div class="col">
								Data: <span>00/00/0000</span>
							</div>
							<div class="col">
								Horário: <span>00:00</span>
							</div>
							<div class="col">
								Local: <span>rua endereço</span>
							</div>
							<div class="col">
								O que vai aprender: 
							</div>
							<div class="col">
								Incluso...
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	<!--=================================
	page-container-->

<?php
	include 'footer.php';
?>