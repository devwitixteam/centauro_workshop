<?php
header('Access-Control-Allow-Origin: *');
$postdata = file_get_contents("php://input");
$request = json_decode($postdata);

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require '../php-mailer/src/Exception.php';
require '../php-mailer/src/PHPMailer.php';
require '../php-mailer/src/SMTP.php';

if(isset($request)) {

	
	$name = $request->name;        // Sender'
	$email = $request->email;      // Sender's email address
	$phone  = $request->phone;     // Sender's phone number
	$message = $request->message;  // Sender's message
	$headers = 'From: CMC - Celestial Medical Cannabis <contato@celestialmed.co>' . "\r\n";
	
	// Import PHPMailer classes into the global namespace
	// These must be at the top of your script, not inside a function
	

	$mail = new PHPMailer(true);                              // Passing `true` enables exceptions
	try {
	    //Server settings
	    $mail->SMTPDebug = 2;                                 // Enable verbose debug output
	    $mail->isSMTP();                                      // Set mailer to use SMTP
	    $mail->Host = 'smtp.celestialmed.co';  // Specify main and backup SMTP servers
	    $mail->SMTPAuth = true;                               // Enable SMTP authentication
	    $mail->Username = 'contato@celestialmed.co';                 // SMTP username
	    $mail->Password = 'SenhaAqui';                           // SMTP password
	    // $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
	    $mail->Port = 587;                                    // TCP port to connect to

	    //Recipients
	    $mail->AddReplyTo($email, $name);
	    $mail->setFrom('celestialmed.co', 'Contato CMC');
	    $mail->addAddress('celestialmed.co', 'Contato CMC');     // Add a recipient
	    // $mail->addAddress('ellen@example.com');               // Name is optional
	    
	    // $mail->addCC('cc@example.com');
	    // $mail->addBCC('bcc@example.com');

	    //Attachments
	    // $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
	    // $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

	    //Content
	    $mail->isHTML(true);                                  // Set email format to HTML
	    $mail->Subject = 'Contato via site';
	    $mail->Body    = $message;
	    $mail->AltBody = $message;

	    $mail->send();
	    echo 'Message has been sent';
	} catch (Exception $e) {
	    echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
	}


 }
