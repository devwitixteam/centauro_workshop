<?php

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST, GET, OPTIONS');

$postdata = file_get_contents("php://input");
$request = json_decode($postdata);

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require '../php-mailer/src/Exception.php';
require '../php-mailer/src/PHPMailer.php';
require '../php-mailer/src/SMTP.php';

if(isset($request)) {

	
	$name = $request->name;        // Sender'
	$email = $request->email;      // Sender's email address
	$phone  = $request->phone;     // Sender's phone number
	$message = $request->message;  // Sender's message
	$headers = 'From: CMC website - celestialmed.com.br>' . "\r\n";
	
	// Import PHPMailer classes into the global namespace
	// These must be at the top of your script, not inside a function
	

	$mail = new PHPMailer(true);                              // Passing `true` enables exceptions
	try {
	    //Server settings
	    $mail->SMTPDebug = 2;                                 // Enable verbose debug output
	    // $mail->isSMTP();                                      // Set mailer to use SMTP
	    $mail->Host = 'localhost';  // Specify main and backup SMTP servers
	    $mail->SMTPAuth = true;                               // Enable SMTP authentication
	    $mail->Username = 'cmc@celestialmed.com.br';                 // SMTP username
	    $mail->Password = 'Maximo12**';                           // SMTP password
	    // $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
	    $mail->Port = 465;                                    // TCP port to connect to

	    //Recipients
	    $mail->AddReplyTo($email, $name);
	    $mail->setFrom($email, $name);
	    $mail->addAddress('contato@celestialmed.com.br', 'Contato CMC');     // Add a recipient
	    // $mail->addAddress('dev@witix.com.br', 'Contato CMC');     // Add a recipient
	    // $mail->addAddress('ellen@example.com');               // Name is optional
	    
	    // $mail->addCC('cc@example.com');
	    // $mail->addBCC('bcc@example.com');

	    //Attachments
	    // $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
	    // $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

	    //Content
	    $mail->isHTML(true);                                  // Set email format to HTML
	    $mail->Subject = 'Contato via site';
	    $mail->Body    = $message;
	    $mail->AltBody = $message;

	    $mail->send();
	    echo 'Mensagem enviada.';

	} catch (Exception $e) {
	    echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
	}


 }
