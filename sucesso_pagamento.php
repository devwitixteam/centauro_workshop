<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="keywords" content="HTML5 Template" />
		<meta name="description" content="Webster - Responsive Multi-purpose HTML5 Template" />
		<meta name="author" content="potenzaglobalsolutions.com" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
		<title>Políticas de Cancelamento</title>

		<!-- Favicon -->
		<link rel="shortcut icon" href="images/favicon.ico" />

		<!-- font -->
		<link  rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:200,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900">
		<link href="https://fonts.googleapis.com/css?family=PT+Serif:400,400i,700,700i" rel="stylesheet">
 
		<!-- Plugins -->
		<link rel="stylesheet" type="text/css" href="template/css/plugins-css.css" />

		<!-- revolution -->
		<link rel="stylesheet" type="text/css" href="template/revolution/css/settings.css" media="screen" />

		<!-- Typography -->
		<link rel="stylesheet" type="text/css" href="template/css/typography.css" />

		<!-- Shortcodes -->
		<link rel="stylesheet" type="text/css" href="template/css/shortcodes/shortcodes.css" />

		<!-- Style -->
		<link rel="stylesheet" type="text/css" href="template/css/style.css" />

		<!-- Responsive -->
		<link rel="stylesheet" type="text/css" href="template/css/responsive.css" /> 

		<!-- Style customizer -->
		<link rel="stylesheet" type="text/css" href="template/css/skins/skin-centauro.css" data-style="styles"/>

		<!-- Style customizer -->
		<link rel="stylesheet" type="text/css" href="assets/css/centauro.css" data-style="styles"/>

		<style>
			.copyright{min-height: 55px; background: #cf3c48; font-size: 1rem;}
    		
    		.links_footer{list-style-type:none; font-size: 1rem;}
    
			.bg-overlay-black-60:before {background:rgba(21, 25, 27, 0.8);}

    		.footer p{font-size: 1rem !important;}

   			.footer a:hover{color:#cf3c48 !important;}

   			.links_politicas{list-style-type: none;}

            .btnHome{background-color: #cf3c48; color: white; border-radius: 5px; font-size: 0.8rem;}

            .btnHome:hover{background-color: transparent; border-color: black; color: black;}

            .icon_sucesso{color: #cf3c48;}

            .text_sucesso{font-size: 1.2rem;}

            .caixa_contato{border: solid 2px #cf3c48; font-size: 1rem;}
            
		</style>
	</head>
	<body>
 	<!--================================= header -->
   		<header id="header" class="header light">
      		<div class="menu" id="onepagenav">  
        	<!-- menu start -->
        		<nav id="menu" class="mega-menu">
          		<!-- menu list items container -->
          			<section class="menu-list-items">
            			<div class="container p-md-0"> 
              				<div class="row"> 
                				<div class="col-lg-12 col-md-12"> 
                  				<!-- menu logo -->
                  					<ul class="menu-logo">
                    					<li>
                      						<a href="#home"><img id="logo_img"  class="img-fluid" src="assets/img/logo.png" alt=""> </a>
                    					</li>
                  					</ul>
                  				<!-- menu links -->
                  					<div class="menu-bar">
					                    <ul class="menu-links">
					                      <li class="active"><a href="index.php">Home</a></li>
					                      <li><a href="index.php #workshop">Sobre o Workshop</a></li>
					                      <li><a href="index.php #diretores">Convidados</a></li>
					                      <li><a href="index.php #centauro">A Centauro</a></li>
					                      <li><a href="index.php #duvidas">Dúvidas frequentes</a></li>
					                      <li><a class="inscrever" href="pagamento.php">Quero me inscrever</a></li>
					                    </ul>
                  					</div>
                				</div>
              				</div>
            			</div>
          			</section>
        		</nav>
        		<!-- menu end -->
      		</div>
    	</header>
		<section class="page-title bg-overlay-black-60 parallax" data-jarallax='{"speed": 0.6}' style="background-image: url(assets/img/bg/bg-5.jpg);">
			<div class="container">
				<div class="row"> 
					<div class="col-lg-12"> 
						<div class="page-title-name">
							<h1 class="m-0">Pagamento Concluído</h1>
						</div>
					</div>
				</div> 
			</div>
		</section>   
        <section>
            <div class="container bg-white p-5 my-0 my-md-3 my-xl-5">
                <div class="row d-flex align-items-end">
                    <div class="col-12 col-lg-6 pr-lg-0">
                        <div class="container skill-counter bg-white  my-0  my-0">
                            <div class="row">
                                <div class="col-12 text-center">
                                    <span class="icon_sucesso">
                                        <i class="fa  fa-thumbs-o-up ft-35"></i>
                                    </span>
                                </div>
                                <div class="col-12 my-2 text-center d-flex justify-content-center">
                                    <div class="section-title line text-sm-left mb-3">
                                        <div class="title">
                                            <h3 class="mb-0">Pagamento efetuado!<span class="d-none d-md-inline"></span></h3>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 mb-4 text-center text_sucesso">
                                    <span>
                                        Obrigada pela participação, em breve você receberá um e-mail da nossa equipe de atendimento.
                                    </span>
                                </div>
                                <div class="col-12 d-flex justify-content-center">
                                   <a href="#"><button class="button btnHome w-100">Ir para Home</button></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-lg-6 mt-2 mt-lg-0 pl-lg-0">
                        <div class="container skill-counter bg-white  my-0  my-0">
                            <div class="row">
                                <div class="col-12 text-center">
                                    <span class="icon_sucesso">
                                        <i class="fa fa-envelope-o ft-35"></i>
                                    </span>
                                </div>
                                <div class="col-12 my-2 text-center d-flex justify-content-center">
                                    <div class="section-title line text-sm-left mb-3">
                                        <div class="title">
                                            <h3 class="mb-0">Entre em contato conosco<span class="d-none d-md-inline"></span></h3>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 mb-4 text_sucesso">
                                    <span>
                                        <b>Zil Ferraz</b>
                                    </span>
                                </div>
                                <div class="col-12 mb-4 text_sucesso">
                                    <span>
                                        <i class="fa fa-envelope-o"></i>
                                    </span>
                                    <span style="word-wrap: break-word;">curso@centauro.com</span>
                                </div>
                                <div class="col-12 mb-3 text_sucesso">
                                    <span>
                                        <i class="fa fa-phone"></i>
                                    </span>
                                    <span>(11) 2227-2527</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section> 	
    	
  <!--================================= header -->		
  		<?php include 'footer.php';?>


	</body>
</html>