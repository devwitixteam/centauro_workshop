$(document).ready(function() {
	  $("#telefone").inputmask({
	    mask: ["(99) 9999-9999"],
	    keepStatic: true
	  });
	  $("#celular").inputmask({
	    mask: ["(99) 9999-9999", "(99) 99999-9999", ],
	    keepStatic: true
	  });

	  $("input[id*='cpf']").inputmask({
		  mask: ['999.999.999-99'],
		  keepStatic: true
	  });

	  $("input[id*='drt']").inputmask({
		  mask: ['9999/999/aa'],
		  keepStatic: true
	  });

	  /*$("#drt").inputmask({
	    mask: ["9999/999/UF"],
	    keepStatic: true
	  });
	  $("#cpf").inputmask({
	    mask: ["999.999.999-99"],
	    keepStatic: true
	  });*/
  });