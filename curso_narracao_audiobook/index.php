<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="keywords" content="HTML5 Template" />
    <meta name="description" content="Centauro" />
    <meta name="author" content="potenzaglobalsolutions.com" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <title>Centauro</title>

    <!-- Favicon -->
    <link rel="shortcut icon" href="images/favicon.ico" />

    <!-- font -->
    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Poppins:200,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900">
    <link href="https://fonts.googleapis.com/css?family=PT+Serif:400,400i,700,700i" rel="stylesheet">

    <!-- Plugins -->
    <link rel="stylesheet" type="text/css" href="../template/css/plugins-css.css" />

    <!-- revolution -->
    <link rel="stylesheet" type="text/css" href="../template/revolution/css/settings.css" media="screen" />

    <!-- Typography -->
    <link rel="stylesheet" type="text/css" href="../template/css/typography.css" />

    <!-- Shortcodes -->
    <link rel="stylesheet" type="text/css" href="../template/css/shortcodes/shortcodes.css" />

    <!-- Style -->
    <link rel="stylesheet" type="text/css" href="../template/css/style.css" />

    <!-- Responsive -->
    <link rel="stylesheet" type="text/css" href="../template/css/responsive.css" />

    <!-- Style customizer -->
    <link rel="stylesheet" type="text/css" href="../template/css/skins/skin-centauro.css" data-style="styles" />

    <!-- Style customizer -->
    <link rel="stylesheet" type="text/css" href="../assets/css/centauro.css" data-style="styles" />



    <style>
        .copyright {
            min-height: 55px;
            background: #cf3c48;
            font-size: 1rem;
        }

        .links_footer {
            list-style-type: none;
            font-size: 1rem;
        }


        .bg-overlay-black-60:before {
            background: rgba(21, 25, 27, 0.8);
        }

        .footer p {
            font-size: 1rem !important;
        }

        .footer a:hover {
            color: #cf3c48 !important;
        }

        .propaganda-carrossel {
            height: 140px;
        }

        .button-garantir {
            background-color: #cf3c48;
        }

        .button-garantir:hover {
            background-color: white;
            color: black !important;
        }

        .action-box a.button {
            margin-top: -13px !important;
        }

        .feature-icon {
            color: #cf3c48;
        }

        .btn-ver {
            background-color: white;
            color: #cf3c48;
            border-radius: 20px;
        }

        .btn-ver:hover {
            background-color: #cf3c48;
            color: white;
            border-color: #cf3c48;
        }

        .modal-video {
            background: transparent !important;
            border: none !important;
        }
    </style>

</head>

<body>

    <div class="wrapper" id="home">
        <!--=================================
   preloader -->
        <div id="pre-loader">
            <img src="../template/images/pre-loader/loader-13.svg" alt="">
        </div>
        <!--=================================
   preloader -->

        <!--=================================
   header -->
        <header id="header" class="header light">
            <div class="menu" id="onepagenav">
                <!-- menu start -->
                <nav id="menu" class="mega-menu">
                    <!-- menu list items container -->
                    <section class="menu-list-items">
                        <div class="container p-md-0">
                            <div class="row">
                                <div class="col-lg-12 col-md-12">
                                    <!-- menu logo -->
                                    <ul class="menu-logo">
                                        <li>
                                            <a href="#home"><img id="logo_img" class="img-fluid"
                                                    src="../assets/img/logo.png" alt=""> </a>
                                        </li>
                                    </ul>
                                    <!-- menu links -->
                                    <div class="menu-bar">
                                        <ul class="menu-links">
                                            <li class="active"><a href="#home">Home</a></li>
                                            <li><a href="#curso">Sobre Curso</a></li>
                                            <li><a href="#diretores">Convidados</a></li>
                                            <li><a href="#centauro">A Centauro</a></li>
                                            <li><a href="#duvidas">Dúvidas frequentes</a></li>
                                            <li><a class="inscrever" href="pagamento.php">Quero me inscrever</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </nav>
                <!-- menu end -->
            </div>
        </header>
        <!--=================================
         header -->

        <!--=================================
          banner -->
        <section class="rev-slider" id="section-video">
            <!--<div class="videoSlide slider-parallax video-background-banner bg-overlay-black-60 parallax" data-jarallax="{&quot;speed&quot;: 0.6}" style="background-image: none; background-attachment: scroll; background-size: auto;" data-jarallax-video="mp4:../assets/video/centauro.mp4,webm:video/video.webm,ogv:video/video.ogv"></div>!-->
            <div class="videoSlide slider-parallax video-background-banner parallax"><img class="img-fluid full-width"
                    src="../template/images/curso/bg_banner_audiobook.jpg" alt=""></div>

            <div id="rev_slider_275_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container"
                data-alias="webster-slider-10" data-source="gallery"
                style="margin:0px auto;background:transparent;padding:0px;margin-top:0px;margin-bottom:0px;">
                <!-- START REVOLUTION SLIDER 5.4.6.3 fullwidth mode -->
                <div id="rev_slider_275_1" class="rev_slider fullwidthabanner" style="display:none;"
                    data-version="5.4.6.3">
                    <ul>
                        <!-- SLIDE  -->
                        <li id="liSlide" data-index="rs-769" data-transition="fade" data-slotamount="default"
                            data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default"
                            data-easeout="default" data-masterspeed="300" data-thumb="" data-delay="7000"
                            data-rotate="0" data-saveperformance="off" data-title="Slide" data-param1="" data-param2=""
                            data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8=""
                            data-param9="" data-param10="" data-description="">
                            <!-- MAIN IMAGE -->
                            <img src="../assets/img/bg/microfone.azul.jpg" data-bgcolor='#141414'
                                style='background:#141414' alt="" data-bgposition="center center" data-bgfit="cover"
                                data-bgrepeat="no-repeat" data-bgparallax="off" class="rev-slidebg" data-no-retina>
                            <!-- LAYERS -->

                            <!-- LAYER NR. 1-->
                            <div class="tp-caption txt-1 tp-resizeme mb-110" id="slide-769-layer-8" data-x="center"
                                data-hoffset="" data-y="300" data-width="['auto']" data-height="['auto']"
                                data-type="text" data-responsive_offset="on"
                                data-frames='[{"delay":300,"split":"chars","splitdelay":0.05,"speed":1900,"split_direction":"forward","frame":"0","from":"y:[100%];z:0;rZ:-35deg;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                                data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]"
                                data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]"
                                data-paddingleft="[0,0,0,0]"
                                style="z-index: 5; white-space: nowrap; font-size: 3rem; line-height: 3rem; font-weight: 400; color: #ffffff; letter-spacing: 10px;text-transform:uppercase;">
                                Narração
                            </div>

                            <!-- LAYER NR. 2 -->
                            <div class="tp-caption txt-2 tp-resizeme" id="slide-769-layer-9" data-x="center"
                                data-hoffset="" data-y="370" data-width="['auto']" data-height="['auto']"
                                data-type="text" data-responsive_offset="on"
                                data-frames='[{"delay":1000,"split":"chars","splitdelay":0.05,"speed":2000,"split_direction":"forward","frame":"0","from":"y:[100%];z:0;rZ:-35deg;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                                data-textAlign="['center','center','center','center']" data-paddingtop="[0,0,0,0]"
                                data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]"
                                data-paddingleft="[0,0,0,0]"
                                style="z-index: 6; white-space: nowrap; font-size: 4rem; line-height: 4.2rem; font-weight: 600; color: #ffffff; letter-spacing: 0px; text-transform: uppercase;">
                                Audiobook
                            </div>

                            <!-- LAYER NR. 27 -->
                            <a class="tp-caption rev-btn txt-3 tp-resizeme rev-button" href="pagamento.php"
                                target="_self" id="slide-769-layer-44" data-x="center" data-hoffset="-1" data-y="370"
                                data-voffset="110" data-width="['auto']" data-height="['auto']" data-type="button"
                                data-actions='' data-responsive_offset="on"
                                data-frames='[{"delay":2400,"speed":2000,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"300","ease":"Power0.easeInOut","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgb(255,255,255);bg:rgb(10,10,10);bs:solid;bw:0 0 0 0;"}]'
                                data-textAlign="['inherit','inherit','inherit','inherit']"
                                data-paddingtop="[12,12,12,12]" data-paddingright="[35,35,35,35]"
                                data-paddingbottom="[12,12,12,12]" data-paddingleft="[35,35,35,35]"
                                style="z-index: 31; white-space: nowrap; letter-spacing: 0.2rem; font-size: 1.5rem; line-height: 1.7rem; font-weight: 700; color: rgba(255,255,255,1); text-transform:uppercase; border-color:rgba(0,0,0,1);border-radius:3px 3px 3px 3px;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;text-decoration: none;">Garantir
                                minha vaga
                            </a>
                        </li>
                    </ul>
                    <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
                </div>
            </div>
        </section>
        <!--=================================
          banner -->

        <!--=================================
          Frase e Video -->
        <section class="action-box bg-white py-5 px-0 full-width">
            <div class="container px-0 px-sm-5 px-lg-3 px-xl-0 propaganda-carrossel">
                <div class="row">
                    <div class="col-12 col-md text-center text-md-left">
                        <h3 class="text-dark my-md-4">Vagas limitadas</h3>
                        <p class="text-dark">Não perca tempo, agarre já essa oportunidade única! </p>
                    </div>
                    <div class="col-12 col-md-4 col-lg-3 text-center px-0 align-self-center mt-3">
                        <a class="button text-white px-2 button-garantir" href="pagamento.php">
                            <span class="text-garantir"><b>Garantir minha vaga</b></span>
                        </a>
                    </div>
                </div>
            </div>
        </section>
        <!--=================================
          Frase e Video -->

        <!--=================================
          Sobre o curso -->
        <section class="clearfix" id="curso">
            <div class="d-none d-md-block page-section-ptb bg-overlay-black-30 parallax" data-jarallax='{"speed": 0.6}'
                style="background-image: url(../assets/img/bg/bg-3.jpg);">
            </div>
            <div class="page-section-pb pt-4 mt-1 mt-md-0">
                <div class="container px-0 px-sm-3 px-lg-5 px-xl-0 mt-5 mt-md-0">
                    <div class="skill-counter px-0 px-sm-3 px-md-5 py-5 pb-0">
                        <div class="row m-0">
                            <div class="col-12">
                                <div class="section-title line left text-center text-sm-left">
                                    <h2 class="title">O Curso </h2>
                                </div>
                                <p class="">Loren 
                                </p>
                            </div>
                            <div class="col-12 mb-4">
                                <div class="row py-md-4">
                                    <div class="col-12 col-lg-4 mt-4">
                                        <div class="feature-text left-icon">
                                            <div class="feature-icon">
                                                <i class="ti-microphone-alt"></i>
                                            </div>
                                            <div class="feature-info">
                                                <h5>Loren </h5>
                                                <div class="modal-body p-1">
                                                    <ul class="list list-unstyled mb-30">
                                                        <li><a href="javascript:void(0)"><i
                                                                    class="fa fa-check-circle"></i></a>Loren </li>
                                                        <li><a href="javascript:void(0)"><i
                                                                    class="fa fa-check-circle"></i></a>Loren </li>
                                                        <li><a href="javascript:void(0)"><i
                                                                    class="fa fa-check-circle"></i></a>Loren </li>
                                                        <li><a href="javascript:void(0)"><i
                                                                    class="fa fa-check-circle"></i></a>Loren </li>
                                                        <li><a href="javascript:void(0)"><i
                                                                    class="fa fa-check-circle"></i></a>Loren </li>
                                                        <li><a href="javascript:void(0)"><i
                                                                    class="fa fa-check-circle"></i></a>Loren </li>
                                                        <li><a href="javascript:void(0)"><i
                                                                    class="fa fa-check-circle"></i></a>Loren </li>
                                                        <li><a href="javascript:void(0)"><i
                                                                    class="fa fa-check-circle"></i></a> Loren </li>
                                                        <li><a href="javascript:void(0)"><i
                                                                    class="fa fa-check-circle"></i></a>Loren </li>
                                                        <li><a href="javascript:void(0)"><i
                                                                    class="fa fa-check-circle"></i></a>Loren </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-lg-4 mt-4">
                                        <div class="feature-text left-icon">
                                            <div class="feature-icon">
                                                <i class="ti-hand-open"></i>
                                            </div>
                                            <div class="feature-info">
                                                <h5 class="">Loren </h5>
                                                <div class="modal-body p-1">
                                                    <ul class="list list-unstyled mb-30">
                                                        <li><a href="javascript:void(0)"><i
                                                                    class="fa fa-check-circle"></i></a>Loren </li>
                                                        <li><a href="javascript:void(0)"><i
                                                                    class="fa fa-check-circle"></i></a>Loren </li>
                                                        <li><a href="javascript:void(0)"><i
                                                                    class="fa fa-check-circle"></i></a>Loren </li>
                                                        <li><a href="javascript:void(0)"><i
                                                                    class="fa fa-check-circle"></i></a>Loren 
                                                        </li>
                                                        <li><a href="javascript:void(0)"><i
                                                                    class="fa fa-check-circle"></i></a>Loren </li>
                                                        <li><a href="javascript:void(0)"><i
                                                                    class="fa fa-check-circle"></i></a>Loren </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-lg-4 mt-4">
                                        <div class="feature-text left-icon">
                                            <div class="feature-icon">
                                                <i class="ti-pin-alt"></i>
                                            </div>
                                            <div class="feature-info">
                                                <h5>Loren Loren Loren </h5>
                                                <div class="modal-body p-1">
                                                    <ul class="list list-unstyled mb-30">
                                                        <li><a href="javascript:void(0)"><i
                                                                    class="fa fa-check-circle"></i></a>Loren Loren Loren </li>
                                                        <li><a href="javascript:void(0)"><i
                                                                    class="fa fa-check-circle"></i></a>Loren Loren Loren </li>
                                                        <li><a href="javascript:void(0)"><i
                                                                    class="fa fa-check-circle"></i></a>Loren Loren Loren </li>
                                                        <li><a href="javascript:void(0)"><i
                                                                    class="fa fa-check-circle"></i></a>Loren Loren Loren </li>
                                                        <li><a href="javascript:void(0)"><i
                                                                    class="fa fa-check-circle"></i></a>Loren Loren Loren 
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 mb-4 mb-md-0">
                                <div class="section-title line left text-center text-sm-left">
                                    <div class="title">
                                        <h3 class="mb-0">Loren Loren <span class="d-none d-md-inline">:</span>
                                        </h3>
                                    </div>
                                </div>
                                <div class="row mt-30">
                                    <div class="col-sm-12">
                                        <ul class="list list-unstyled list-hand">
                                            <li>Loren Loren </li>
                                            <li>Loren Loren </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-6">
                                <div class="section-title line left text-center text-sm-left">
                                    <div class="title">
                                        <h3 class="mb-0">Loren <span class="d-none d-md-inline">:</span></h3>
                                    </div>
                                </div>
                                <div class="row mt-30">
                                    <div class="col-sm-12">
                                        <ul class="list list-unstyled list-hand">
                                            <li>Loren </li>
                                            <li>Loren o</li>
                                            <li>Loren </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--=================================
          Sobre o curso -->

        <!--=================================
          Box call action -->
        <section class="action-box px-0 pattern py-3 py-sm-5 full-width"
            style="background: url(../assets/img/bg/patten.jpg) repeat;">
            <div class="container px-0 px-lg-3 px-xl-0">
                <div class="row">
                    <div class="col-12 col-md text-center text-md-left">
                        <h3>Vagas limitadas</h3>
                        <p>Não perca tempo, agarre já essa oportunidade única!</p>
                    </div>
                    <div class="col-12 col-md-4 col-lg-3 text-center px-0 align-self-center mt-3">
                        <a class="button button-border black px-2" href="pagamento.php">
                            <span><b>Garantir minha vaga</b></span>
                        </a>
                    </div>
                </div>
            </div>
        </section>
        <!--=================================
          Box call action -->

        <!--=================================
          Diretores convidados-->
        <section class="page-section-ptb" id="diretores">
            <div class="container px-sm-4 px-lg-5 px-xl-0">
                <div class="section-title line left text-center text-sm-left">
                    <h2 class="title">Convidados</h2>
                </div>
                <div class="team-3">
                    <div class="row">
                        <div class="col-lg-3 col-sm-6 sm-mb-30">
                            <div class="team team-round full-border">
                                <div class="team-photo px-4 pt-4 px-sm-5 px-lg-2 pt-sm-5 pt-lg-2">
                                    <img class="img-fluid mx-auto" src="../template/images/convidados/julio_franco.png"
                                        alt="">
                                </div>
                                <div class="team-description">
                                    <div class="team-info">
                                        <h5>JÚLIO FRANCO </h5>
                                        <p class="">Narrador</p>
                                        <span>
                                            <a class="button btn-ver" data-toggle="modal" data-target=".diretor2"
                                                href="javascript:void(0)"><b>Ver mais</b></a>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-sm-6 xs-mb-30">
                            <div class="team team-round full-border">
                                <div class="team-photo px-4 pt-4 px-sm-5 px-lg-2 pt-sm-5 pt-lg-2">
                                    <img class="img-fluid mx-auto" src="../template/images/convidados/clara_rocha.png"
                                        alt="">
                                </div>
                                <div class="team-description">
                                    <div class="team-info">
                                        <h5>CLARA ROCHA </h5>
                                        <p class="">Fonoaudióloga</p>
                                        <span>
                                            <a class="button btn-ver" data-toggle="modal" data-target=".diretor3"
                                                href="javascript:void(0)"><b>Ver mais</b></a>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-sm-6 sm-mb-30">
                            <div class="team team-round full-border">
                                <div class="team-photo px-4 pt-4 px-sm-5 px-lg-2 pt-sm-5 pt-lg-2">
                                    <img class="img-fluid mx-auto" src="../template/images/convidados/jorge-destez.png"
                                        alt="">
                                </div>
                                <div class="team-description">
                                    <div class="team-info">
                                        <h5>Jorge Destez</h5>
                                        <p class="">Diretor</p>
                                        <span>
                                            <a class="button btn-ver" data-toggle="modal" data-target=".diretor1"
                                                href="javascript:void(0)"><b>Ver mais</b></a>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-sm-6">
                            <div class="team team-round full-border mb-0">
                                <div class="team-photo px-4 pt-4 px-sm-5 px-lg-2 pt-sm-5 pt-lg-2">
                                    <img class="img-fluid full-width" src="../template/images/convidados/madruga.png"
                                        alt="">
                                </div>
                                <div class="team-description">
                                    <div class="team-info">
                                        <h5>Convidado</h5>
                                        <p class="">Convidado</p>

                                        <span>
                                            <a class="button btn-ver" data-toggle="modal" data-target=".diretor4"
                                                href="javascript:void(0)"><b>Ver mais</b></a>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- modals -->
            <div class="modal p-2 fade diretor1" tabindex="-1">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="row px-4 py-3 px-md-5 py-md-4">
                            <div class="col-12">
                                <button type="button" class="close px-1 py-0" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="col-lg-4 sm-mb-30 text-center">
                                <img class="img-fluid full-width" src="../template/images/convidados/jorge-destez.png"
                                    alt="">
                                <h2 class="theme-color mt-2">Jorge Destez</h2>
                                <h6>Diretor</h6>
                            </div>
                            <div class="col-lg-8">
                                <div class="modal-body p-1">
                                    <!--<h6>Principais personagens</h6>
                            <ul class="list list-unstyled mb-30">
                              <li><a href="javascript:void(0)"><i class="fa fa-check-circle"></i></a> Nome <b>(FILME)</b> </li>
                              <li><a href="javascript:void(0)"><i class="fa fa-check-circle"></i></a> Nome <b>(SÉRIE)</b> </li>
                              <li><a href="javascript:void(0)"><i class="fa fa-check-circle"></i></a> Nome <b>(DOCUMENTÁRIO)</b> </li>
                              <li><a href="javascript:void(0)"><i class="fa fa-check-circle"></i></a> Nome <b>(DESENHO)</b> </li>
                            </ul>!-->
                                    <h6>Carreira</h6>
                                    <ul class="list list-unstyled mb-30">
                                        <li><a href="javascript:void(0)"><i class="fa fa-check-circle"></i></a>Ator,
                                            dublador e diretor de dublagem desde 1993</li>
                                        <li><a href="javascript:void(0)"><i class="fa fa-check-circle"></i></a>Em 2013,
                                            atuou como Leão na novela da Record "Pecado Mortal" e, em 2015, na mesma
                                            emissora, interpretou Baltazar na minissérie "Milagre de Jesus"</li>
                                    </ul>
                                    <h6>Principais Personagens</h6>
                                    <ul class="list list-unstyled mb-30">
                                        <li><a href="javascript:void(0)"><i class="fa fa-check-circle"></i></a>Ace das
                                            Meninas Super Poderosas</li>
                                        <li><a href="javascript:void(0)"><i class="fa fa-check-circle"></i></a>Sideburn
                                            em Transformers: A nova geração</li>
                                        <li><a href="javascript:void(0)"><i class="fa fa-check-circle"></i></a>Ogremon
                                            em Digimon</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal p-2 fade diretor2" tabindex="-1">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="row px-4 py-3 px-md-5 py-md-4">
                            <div class="col-12">
                                <button type="button" class="close px-1 py-0" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="col-lg-4 sm-mb-30 text-center">
                                <img class="img-fluid full-width" src="../template/images/convidados/julio_franco.png"
                                    alt="">
                                <h2 class="theme-color m-1 mt-2">Júlio Franco</h2>
                                <h6>Narrador</h6>
                            </div>
                            <div class="col-lg-8">
                                <div class="modal-body p-1">
                                    <h6>Carreira</h6>
                                    <ul class="list list-unstyled mb-30">
                                        <li><a href="javascript:void(0)"><i class="fa fa-check-circle"></i></a>Mais de
                                            25 anos de carreira como Narrador, Locutor, Apresentador, com mais de 1.730
                                            documentários narrados para os Canais NatGeo- National Geographic Channel,
                                            Discovery Networks e History Channel</li>
                                        <li><a href="javascript:void(0)"><i
                                                    class="fa fa-check-circle"></i></a>Palestrante, Mentor em Oralidade
                                            e Oratória</li>
                                    </ul>
                                    <h6>Principais Programas</h6>
                                    <ul class="list list-unstyled mb-30">
                                        <li><a href="javascript:void(0)"><i
                                                    class="fa fa-check-circle"></i></a>FOTOGRAFIA - Narração do Curso em
                                            DVD</li>
                                        <li><a href="javascript:void(0)"><i class="fa fa-check-circle"></i></a>Começa o
                                            Mundial de F1 de 2013</li>
                                        <li><a href="javascript:void(0)"><i class="fa fa-check-circle"></i></a>A
                                            Fotografia suas Belezas e Histórias</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal p-2 fade diretor3" tabindex="-1">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="row px-4 py-3 px-md-5 py-md-4">
                            <div class="col-12">
                                <button type="button" class="close px-1 py-0" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="col-lg-4 sm-mb-30 text-center">
                                <img class="img-fluid full-width" src="../template/images/convidados/clara_rocha.png"
                                    alt="">
                                <h2 class="theme-color mt-2">Clara Rocha</h2>
                                <h6>Fonoaudióloga</h6>
                            </div>
                            <div class="col-lg-8">
                                <div class="modal-body p-1">
                                    <h6>Carreira</h6>
                                    <ul class="list list-unstyled mb-30">
                                        <li><a href="javascript:void(0)"><i
                                                    class="fa fa-check-circle"></i></a>Fonoaudióloga, consultora em
                                            comunicação e preparadora vocal de artistas e comunicadores</li>
                                        <li><a href="javascript:void(0)"><i class="fa fa-check-circle"></i></a>Atriz,
                                            dubladora e locutora publicitária</li>
                                    </ul>
                                    <h6>Formação e Experiência Profissional</h6>
                                    <ul class="list list-unstyled mb-30">
                                        <li><a href="javascript:void(0)"><i class="fa fa-check-circle"></i></a>Mestra em
                                            Artes da Cena pela UNICAMP</li>
                                        <li><a href="javascript:void(0)"><i
                                                    class="fa fa-check-circle"></i></a>Especialista em voz pelo CFFa
                                        </li>
                                        <li><a href="javascript:void(0)"><i
                                                    class="fa fa-check-circle"></i></a>Fonoaudióloga formada pela
                                            UNIFESP – Universidade Federal de São Paulo</li>
                                        <li><a href="javascript:void(0)"><i
                                                    class="fa fa-check-circle"></i></a>Fonoaudióloga e coach no espaço
                                            Núcleo Vocal</li>
                                        <li><a href="javascript:void(0)"><i
                                                    class="fa fa-check-circle"></i></a>Professora de Técnica e Expressão
                                            Vocal em escolas de teatro, teatro musical e dublagem</li>
                                        <li><a href="javascript:void(0)"><i
                                                    class="fa fa-check-circle"></i></a>Professora de Voz e Habilidades
                                            Comunicativas da Escola de Comunicadores SP (ECOSP), em cursos voltado para
                                            apresentadores de TV, mídias digitais e demais comunicadores</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal p-2 fade diretor4" tabindex="-1">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="row px-4 py-3 px-md-5 py-md-4">
                            <div class="col-12">
                                <button type="button" class="close px-1 py-0" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="col-lg-4 sm-mb-30 text-center">
                                <div id="carousel-convidado" class="carousel slide" data-ride="carousel">
                                    <div class="carousel-inner">
                                        <div class="carousel-item active">
                                            <img class="d-block w-100" src="../template/images/convidados/madruga.png"
                                                alt="">
                                        </div>
                                        <div class="carousel-item">
                                            <img class="d-block w-100" src="../template/images/convidados/madruga_2.png"
                                                alt="">
                                        </div>
                                        <div class="carousel-item">
                                            <img class="d-block w-100" src="../template/images/convidados/stan_lee.png"
                                                alt="">
                                        </div>
                                    </div>
                                </div>
                                <h2 class="theme-color mt-2">Convidado</h2>
                                <h6>Convidado</h6>
                            </div>
                            <div class="col-lg-8">
                                <div class="modal-body p-1">
                                    <h6>LOREM</h6>
                                    <ul class="list list-unstyled mb-30">
                                        <li><a href="javascript:void(0)"><i class="fa fa-check-circle"></i></a>LOREM</li>
                                        <li><a href="javascript:void(0)"><i class="fa fa-check-circle"></i></a>LOREM</li>
                                    </ul>
                                    <h6>LOREM</h6>
                                    <ul class="list list-unstyled mb-30">
                                        <li><a href="javascript:void(0)"><i class="fa fa-check-circle"></i></a>LOREM</li>
                                        <li><a href="javascript:void(0)"><i class="fa fa-check-circle"></i></a>LOREM</li>
                                        <li><a href="javascript:void(0)"><i class="fa fa-check-circle"></i></a>LOREM
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- modals -->
        </section>
        <!--=================================
          Diretores convidados-->

        <!--=================================
          Sobre a centauro-->
        <section id="centauro" class="page-section-ptb bg-overlay-black-70 parallax position-relative"
            data-jarallax='{"speed": 0.6}' style="background: url(../assets/img/bg/bg-4.jpg);">
            <div class="container px-0 px-sm-4 px-lg-5 px-xl-0">
                <div class="row m-0 d-flex align-items-center">
                    <div class="col-12 col-md-6 mb-4 mb-md-0 order-2 order-md-1">
                        <!--<div class="owl-carousel" data-nav-arrow="true" data-smartspeed="1200" data-items="1" data-md-items="1" data-sm-items="1" data-xs-items="1" data-xx-items="1">
                    <div class="item"><img class="img-fluid full-width" src="../template/images/about/01.jpg" alt="">
                    </div>
                    <div class="item"><img class="img-fluid full-width" src="../template/images/about/02.jpg" alt="">
                    </div>
                    <div class="item"><img class="img-fluid full-width" src="../template/images/about/03.jpg" alt="">
                    </div>
                  </div>!-->
                        <div class="popup-video mb-0">
                            <img class="img-fluid full-width" src="../template/images/centauro/fundo_video.jpg" alt="">
                            <div class="play-video text-center">
                                <a class="view-video" data-toggle="modal" data-target="#play-video-centauro"> <i
                                        class="fa fa-play" style="color: white"></i> </a>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-md-6 order-1 order-md-2">
                        <div class="section-title line left text-center text-sm-left">
                            <h2 class="title text-white">A Centauro</h2>
                        </div>
                        <p class="text-white text-left d-none d-md-block mb-0">A Centauro é pioneira em Dublagem e
                            Legendagem para diversos conteúdos audiovisuais, dentre eles: longas-metragens, séries,
                            novelas, documentários e muito, muito mais.</p>
                        <a class="button button-border-white mt-30 d-none d-md-block"
                            href="http://centauro.com/dublagem" target="_blank"><span>Conheça a Centauro</span></a>
                    </div>
                    <div class="col-12 order-3 text-center">
                        <p class="text-white text-left d-md-none">A Centauro é pioneira em Dublagem e Legendagem para
                            diversos conteúdos audiovisuais, dentre eles: longas-metragens, séries, novelas,
                            documentários e muito, muito mais.</p>
                        <a class="button button-border-white mt-30 d-md-none" href="http://centauro.com/dublagem"
                            target="_blank"><span>Conheça a Centauro</span></a>
                    </div>
                </div>
            </div>
        </section>

        <div class="modal fade" id="play-video-centauro" tabindex="-1" role="dialog"
            aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div class="modal-content modal-video p-0" style="max-width: none">
                    <div class="modal-body p-0" style="position: relative;">
                        <button type="button" class="btn btn-secondary mb-2" data-dismiss="modal"
                            style="position:absolute; background-color: transparent; border:none;top: -40px; right: 0; font-size: 1.5rem;">X</button>
                        <video width="100%" controls autoplay>
                            <source
                                src="https://s3-sa-east-1.amazonaws.com/centauro-com/pt-br/apresentacao-centauro-480p.mp4"
                                type="video/mp4">
                        </video>
                    </div>
                </div>
            </div>
        </div>

        <!--=================================
          Sobre a centauro-->

        <!--=================================
          Investimento-->
        <section id="investimento" class="o-hidden">
            <div class="container px-0 px-sm-4 px-lg-5 px-xl-0 py-5">
                <div class="row m-0">
                    <div class="col">
                        <div class="section-title line left text-center text-sm-left">
                            <h2 class="title">Investimento</h2>
                        </div>
                        <p class="mb-0">
                            Pague o curso com segurança usando o Pag Seguro! Para sua comodidade, o valor pode ser
                            dividido em até 3 vezes.<laboris class=""></laboris>
                        </p>
                    </div>
                    <div class="col-12">
                        <div class="row mt-30 d-flex align-items-center">
                            <div class="col-lg-6 text-center text-lg-left">
                                <div class="row d-flex align-items-center justify-content-center">
                                    <div class="col pr-0">
                                        <img class="full-width"
                                            src="//assets.pagseguro.com.br/ps-integration-assets/banners/pagamento/avista_estatico_550_70.gif/assets.pagseguro.com.br/ps-integration-assets/banners/pagamento/avista_estatico_550_70.gif"
                                            alt="Logotipos de meios de pagamento do PagSeguro"
                                            title="Este site aceita pagamentos com as principais bandeiras e bancos, saldo em conta PagSeguro e boleto.">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 text-center text-lg-left mt-4 mt-lg-0">
                                <div class="row d-flex align-items-center justify-content-center">
                                    <div class="col text-lg-right">
                                        <a class="button text-white px-2 button-garantir" href="pagamento.php">
                                            <span class="text-garantir"><b>Garantir com cartão ou boleto</b></span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--=================================
          Investimento-->

        <!--=================================
          Duvidas-->
        <section class="clearfix" id="duvidas">
            <div class="d-none d-md-block page-section-ptb bg-overlay-black-30 parallax" data-jarallax='{"speed": 0.6}'
                style="background-image: url(../assets/img/bg/bg-4.jpg);">
            </div>
            <div class="page-section-pb mt-2 mt-md-0 pt-4">
                <div class="container px-0 px-sm-3 px-lg-5 px-xl-0 mt-5 mt-md-0">
                    <div class="skill-counter px-3 px-md-5 py-5">
                        <div class="row">
                            <div class="col-12">
                                <div class="section-title line left text-center text-sm-left mb-0">
                                    <h2 class="title">Dúvidas Frequentes</h2>
                                </div>
                                <div class="accordion animated px-sm-3 mt-0">
                                    <div class="acd-group">
                                        <a href="#" class="acd-heading">Loren</a>
                                        <p class="acd-des">Loren</p>
                                    </div>
                                    <div class="acd-group">
                                        <a href="#" class="acd-heading">Loren</a>
                                        <p class="acd-des">Loren</p>
                                    </div>
                                    <div class="acd-group">
                                        <a href="#" class="acd-heading">Loren</a>
                                        <p class="acd-des">Loren</p>
                                    </div>
                                    <div class="acd-group">
                                        <a href="#" class="acd-heading">Loren</a>
                                        <p class="acd-des">Loren</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--=================================
          Duvidas-->

        <!--=================================
          Box call action -->
        <section class="action-box bg-red py-5 px-0 full-width">
            <div class="container px-0 px-sm-5 px-lg-3 px-xl-0">
                <div class="row">
                    <div class="col-12 col-md text-center text-md-left">
                        <h3 class="text-white">Vagas limitadas</h3>
                        <p class="text-white">Não perca tempo, agarre já essa oportunidade única!</p>
                    </div>
                    <div class="col-12 col-md-4 col-lg-3 text-center px-0 align-self-center mt-3">
                        <a class="button button-border white px-2" href="pagamento.php">
                            <span><b>Garantir minha vaga</b></span>
                        </a>
                    </div>
                </div>
            </div>
        </section>
        <!--=================================
          Box call action -->

        <!--=================================
          Fale conosco -->
        <section id="faleconosco" class="page-section-ptb contact-2">
            <div class="container px-0 px-sm-4 px-lg-5 px-xl-0">
                <div class="row m-0 justify-content-center text-center">
                    <div class="col-12">
                        <div class="section-title line left text-center text-sm-left">
                            <h2 class="title">Fale conosco</h2>
                        </div>
                    </div>
                </div>
                <div class="row m-0">
                    <div class="col-12">
                        <div id="formmessage">Success/Error Message Goes Here</div>
                        <form id="contactform" role="form" method="post" action="../template/php/contact-form.php">
                            <div class="contact-form clearfix row">
                                <div class="col-12 col-md-6">
                                    <div class="section-field w-100">
                                        <input id="nome" type="text" placeholder="Nome*" class="form-control"
                                            name="nome" required="required">
                                    </div>
                                </div>
                                <div class="col-12 col-sm-7 col-md-6">
                                    <div class="section-field w-100">
                                        <input type="email" placeholder="E-mail*" class="form-control" name="email"
                                            required="required">
                                    </div>
                                </div>
                                <div class="col-12 col-sm-5 col-md-6">
                                    <div class="section-field w-100">
                                        <input type="text" placeholder="Telefone*" class="form-control" name="telefone"
                                            required="required">
                                    </div>
                                </div>
                                <div class="col-12 col-md-6">
                                    <div class="section-field w-100">
                                        <input type="text" placeholder="Assunto*" class="form-control" name="assunto"
                                            required="required">
                                    </div>
                                </div>
                                <div class="col-12 section-field textarea">
                                    <textarea class="input-message form-control" placeholder="Comentário*" rows="7"
                                        name="message" required="required"></textarea>
                                </div>
                                <!-- Google reCaptch-->
                                <!-- <div class="g-recaptcha section-field clearfix" data-sitekey="[Add your site key]"></div> -->
                                <div class="col-12 section-field submit-button w-100 text-center">
                                    <input type="hidden" name="action" value="sendEmail" />
                                    <button id="submit" name="submit" type="submit" value="Send"
                                        class="button w-100"><span> Enviar mensagem </span> <i
                                            class="fa fa-paper-plane"></i></button>
                                </div>
                            </div>
                        </form>
                        <div id="ajaxloader" style="display:none"><img class="mx-auto mt-30 mb-30 d-block"
                                src="../template/images/pre-loader/loader-02.svg" alt=""></div>
                    </div>
                </div>
            </div>
        </section>
        <!--=================================
          Fale conosco -->

        <?php
    include 'footer.php';
?>