
  <div id="back-to-top"><a class="top arrow" href="#top"><i class="fa fa-angle-up"></i> <span>TOPO</span></a></div>
  <!--=================================
   footer -->
    <footer class="footer transparent footer-topbar page-section-pt bg-overlay-black-60 parallax" data-jarallax='{"speed": 0.6}' style="background-image: url(template/images/centauro/estudio.jpg);">
      <div class="container footer">
        <div class="row">
           <div class="col-12 col-lg-4 align-self-center">
              <img src="../template/images/centauro/logo_b.png" class="mx-auto d-block" alt="">
          </div>
          <div class="col-12 col-lg-8 mt-4 mt-lg-0">
            <p class="text-centauro ">
              A Centauro é uma organização full service e oferece serviços que abrangem produções audiovisuais e desenvolvimento web, atuamos em indústrias cinematográficas, televisivas, VOD, plataformas de videogames, comunicação empresarial e publicidade, proporcionando serviços de alta qualidade em um só lugar
            </p>
          </div>
        </div>
        <div class="row mt-4 py-4 d-flex justify-content-between border-top text-center">
          <div class="col-sm-6 col-md-3 col-xl-auto my-1 px-xl-0">
            <ul class="links_footer">
              <span><li><a href=#>Home</a></li></span>
            </ul>
          </div>
          <div class="col-sm-6 col-md-3 col-xl-auto my-1 px-xl-0">
            <ul class="links_footer">
              <span><li><a href="#">Sobre o Workshop</a></li></span>
            </ul>
          </div>
          <div class="col-sm-6 col-md-3 col-xl-auto my-1 px-xl-0">
            <ul class="links_footer">
              <span><li><a href="">Convidados</a></li></span>
            </ul>
          </div>
          <div class="col-sm-6 col-md-3 col-xl-auto my-1 px-xl-0">
            <ul class="links_footer">
              <span><li><a href="#">A Centauro</a></li></span>
            </ul>
          </div>
          <div class="col-sm-6 col-md-4 col-xl-auto my-1 px-md-0 px-lg-3 px-xl-0">
            <ul class="links_footer">
              <span><li><a href="">Dúvidas Frequentes</a></li></span>
            </ul>
          </div>
          <div class="col-sm-6 col-md-4 col-xl-auto my-1 px-md-0 px-lg-3 px-xl-0">
            <ul class="links_footer">
              <span><li><a href="#">Quero me inscrever</a></li></span>
            </ul>
          </div>
          <div class="col-sm-12 col-md-4 col-xl-auto my-1 px-md-0 px-lg-3 px-xl-0">
            <ul class="links_footer">
              <span><li><a href="#">Políticas de cancelamento</a></li></span>
            </ul>
          </div>
        </div>
      </div>
    </footer>
    <div class="container-fluid copyright">
      <div class="row text-white">
        <div class="col-12 col-lg-8 mt-3  text-center">
          <span>© 2018 Centauro Comunicaciones. Todos os direitos reservados
          </span>
        </div>
        <div class="col-12 col-lg-4 mt-3 mb-3 text-center text-lg-left">
          <span>Siga-nos</span>
          <span class="ml-2"><a href="https://www.facebook.com/centaurodublagembrasil/"><i class="ti-facebook text-white"></i></a></span>
          <span class="ml-2"><a href="https://www.instagram.com/centaurocomunicacionesbrasil/"><i class="ti-instagram text-white"></i></a></span>
          <span class="ml-2"><a href="https://pt.linkedin.com/company/centauro-comunicaciones"><i class="ti-linkedin text-white"></i></a></span>
        </div>
      </div>
    </div>
  <!--=================================
   footer -->
</div>

<!--=================================
 jquery -->

<!-- jquery -->
<script src="../template/js/jquery-3.3.1.min.js"></script>

<!-- plugins-jquery -->
<script src="../template/js/plugins-jquery.js"></script>

<!-- plugin_path -->
<script>var plugin_path = 'template/js/';</script>

<!-- REVOLUTION JS FILES -->
<script src="../template/revolution/js/jquery.themepunch.tools.min.js"></script>
<script src="../template/revolution/js/jquery.themepunch.revolution.min.js"></script>

<!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->
<script src="../template/revolution/js/extensions/revolution.extension.actions.min.js"></script>
<script src="../template/revolution/js/extensions/revolution.extension.carousel.min.js"></script>
<script src="../template/revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
<script src="../template/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
<script src="../template/revolution/js/extensions/revolution.extension.migration.min.js"></script>
<script src="../template/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
<script src="../template/revolution/js/extensions/revolution.extension.parallax.min.js"></script>
<script src="../template/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
<script src="../template/revolution/js/extensions/revolution.extension.video.min.js"></script>
<!-- revolution custom --> 
<script src="../template/revolution/js/revolution-custom.js"></script>
 
<!-- custom -->
<script src="../template/js/custom.js"></script>

<script type="text/javascript" src="../assets/js/mascara.js"></script>
<script type="text/javascript" src="../assets/js/jquery.inputmask.bundle.js"></script>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v3.1';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
 
 
</body>
</html>