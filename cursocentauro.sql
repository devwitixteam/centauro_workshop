-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 16-Jul-2019 às 14:38
-- Versão do servidor: 10.1.32-MariaDB
-- PHP Version: 5.6.36

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cursocentauro`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL,
  `nome` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `celular` varchar(15) NOT NULL,
  `telefone` varchar(15) DEFAULT NULL,
  `cpf` varchar(15) NOT NULL,
  `drt` varchar(15) NOT NULL,
  `status` int(11) DEFAULT NULL,
  `codigo_formulario` varchar(100) DEFAULT NULL,
  `codigo_transacao` varchar(50) DEFAULT NULL,
  `data_cadastro` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `usuarios`
--

INSERT INTO `usuarios` (`id`, `nome`, `email`, `celular`, `telefone`, `cpf`, `drt`, `status`, `codigo_formulario`, `codigo_transacao`, `data_cadastro`) VALUES
(12, 'Teste', 'Teste@Witix.com.br', '(00) 00000-0000', '(00) 0000-0000', '000.000.000-00', '0000/000/DD', NULL, NULL, NULL, '2019-07-15'),
(13, 'Teste Witix', 'teste@witix.com.br', '(00) 00000-0000', '(00) 0000-0000', '000.000.000-__', '0000/000/DD', NULL, 'DEE5AE0CE3E37AEEE4259F83F9088DE9', NULL, '2019-07-15'),
(14, 'Teste Witix', 'Teste@Witix.com.br', '(00) 00000-0000', '(00) 0000-0000', '000.000.000-00', '0000/000/dd', NULL, '80BB413BCECE8D0FF426AFA868692AFC', NULL, '2019-07-15'),
(15, 'teste', 'teste@witix.com.br', '(00) 00000-0000', '(00) 0000-0000', '000.000.000-00', '0000/000/dd', NULL, NULL, NULL, '2019-07-15'),
(16, 'teste', 'teste@witix.com.br', '(00) 00000-0000', '(00) 0000-0000', '000.000.000-00', '0000/000/dd', NULL, NULL, NULL, '2019-07-15'),
(17, 'teste', 'teste@witix.com.br', '(00) 00000-0000', '(00) 0000-0000', '000.000.000-00', '0000/000/dd', NULL, NULL, NULL, '2019-07-15'),
(18, 'Teste Witix', 'teste@witix.com.br', '(14) 5615-6156', '(14) 5615-6156', '222.222.222-22', '2222/222/ff', NULL, NULL, NULL, '2019-07-15'),
(19, 'Teste Witix', 'teste@witix.com.br', '(14) 5615-6156', '(14) 5615-6156', '222.222.222-22', '2222/222/ff', NULL, NULL, NULL, '2019-07-15'),
(20, 'teste teste', 'teste5@teste.com', '(22) 22222-2222', '(22) ____-____', '222.222.222-22', '2222/222/__', NULL, '4887CD7C1212840884CD0F9C7D2B2247', NULL, '2019-07-15'),
(21, 'teste teste', 'teste5@teste.com', '(22) ____-', '(98) 7458-9652', '222.222.222-22', '2222/222/ff', NULL, '36785697FAFAB2A004121F942AF9BCC9', NULL, '2019-07-15'),
(22, 'teste teste', 'teste5@teste.com', '(22) ____-', '(98) 7458-9652', '222.222.222-22', '2222/222/ff', NULL, '9E572FF46666103444543FB22576997A', NULL, '2019-07-15'),
(23, 'Teste', 'Teste@Witix.com.br', '(00) 00000-0000', '(00) 0000-0000', '000.000.000-00', '0000/000/dd', NULL, 'A85F7FA65B5BB06554529FB0C6B08545', NULL, '2019-07-15'),
(24, 'Teste', 'teste@witix.com.br', '(00) 00000-0000', '(00) 0000-0000', '000.000.000-00', '0000/000/dd', NULL, '42E60B0C3434225FF48B9F85232A07D1', NULL, '2019-07-16');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
