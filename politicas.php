<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="keywords" content="HTML5 Template" />
		<meta name="description" content="Webster - Responsive Multi-purpose HTML5 Template" />
		<meta name="author" content="potenzaglobalsolutions.com" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
		<title>Políticas de Cancelamento</title>

		<!-- Favicon -->
		<link rel="shortcut icon" href="images/favicon.ico" />

		<!-- font -->
		<link  rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:200,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900">
		<link href="https://fonts.googleapis.com/css?family=PT+Serif:400,400i,700,700i" rel="stylesheet">
 
		<!-- Plugins -->
		<link rel="stylesheet" type="text/css" href="template/css/plugins-css.css" />

		<!-- revolution -->
		<link rel="stylesheet" type="text/css" href="template/revolution/css/settings.css" media="screen" />

		<!-- Typography -->
		<link rel="stylesheet" type="text/css" href="template/css/typography.css" />

		<!-- Shortcodes -->
		<link rel="stylesheet" type="text/css" href="template/css/shortcodes/shortcodes.css" />

		<!-- Style -->
		<link rel="stylesheet" type="text/css" href="template/css/style.css" />

		<!-- Responsive -->
		<link rel="stylesheet" type="text/css" href="template/css/responsive.css" /> 

		<!-- Style customizer -->
		<link rel="stylesheet" type="text/css" href="template/css/skins/skin-centauro.css" data-style="styles"/>

		<!-- Style customizer -->
		<link rel="stylesheet" type="text/css" href="assets/css/centauro.css" data-style="styles"/>

		<style>
			.copyright{min-height: 55px; background: #cf3c48; font-size: 1rem;}
    		
    		.links_footer{list-style-type:none; font-size: 1rem;}
    
			.bg-overlay-black-60:before {background:rgba(21, 25, 27, 0.8);}

    		.footer p{font-size: 1rem !important;}

   			.footer a:hover{color:#cf3c48 !important;}

   			.links_politicas{list-style-type: none;}
		</style>
	</head>
	<body>
 	<!--================================= header -->
   		<header id="header" class="header light">
      		<div class="menu" id="onepagenav">  
        	<!-- menu start -->
        		<nav id="menu" class="mega-menu">
          		<!-- menu list items container -->
          			<section class="menu-list-items">
            			<div class="container p-md-0"> 
              				<div class="row"> 
                				<div class="col-lg-12 col-md-12"> 
                  				<!-- menu logo -->
                  					<ul class="menu-logo">
                    					<li>
                      						<a href="#home"><img id="logo_img"  class="img-fluid" src="assets/img/logo.png" alt=""> </a>
                    					</li>
                  					</ul>
                  				<!-- menu links -->
                  					<div class="menu-bar">
					                    <ul class="menu-links">
					                      <li class="active"><a href="index.php">Home</a></li>
					                      <li><a href="index.php #workshop">Sobre o Workshop</a></li>
					                      <li><a href="index.php #diretores">Convidados</a></li>
					                      <li><a href="index.php #centauro">A Centauro</a></li>
					                      <li><a href="index.php #duvidas">Dúvidas frequentes</a></li>
					                      <li><a class="inscrever" href="pagamento.php">Quero me inscrever</a></li>
					                    </ul>
                  					</div>
                				</div>
              				</div>
            			</div>
          			</section>
        		</nav>
        		<!-- menu end -->
      		</div>
    	</header>
		<section class="page-title bg-overlay-black-60 parallax" data-jarallax='{"speed": 0.6}' style="background-image: url(assets/img/bg/bg-5.jpg);">
			<div class="container">
				<div class="row"> 
					<div class="col-lg-12"> 
						<div class="page-title-name">
							<h1 class="m-0">Políticas de Cancelamento</h1>
						</div>
					</div>
				</div> 
			</div>
		</section>    	
    	<section id="Politicas">
    		<div class="container politicas bg-white skill-counter p-5 my-0 my-md-3 my-xl-5">
    			<div class="row">
    				<div class="col-12">
    					<div class="section-title line left text-center text-sm-left mb-3">
							<div class="title">
								<h3 class="mb-0">Desistência<span class="d-none d-md-inline"></span></h3>
							</div>
						</div>
					</div>    				
    				<div class="col-12">
    					<ul class="links_politicas">
    						<li>Se o Aluno comunicar a desistência de participação no Workshop com até 15 dias de antecedência da data de sua realização, receberá de volta 50% do valor total. Se a comunicação se der entre 14 e 07 dias de antecedência, receberá 25% do valor total. Se a desistência for comunicada com menos 06 dias de antecedência, não haverá devolução de valores.</li>
    					</ul>
    				</div>
    			</div>
    			<div class="row mt-4">
    				<div class="col-12">
    					<div class="section-title line left text-center text-sm-left mb-3">
							<div class="title">
								<h3 class="mb-0">Cancelamento<span class="d-none d-md-inline"></span></h3>
							</div>
						</div>
					</div>   				
    				<div class="col-12">
    					<ul class="links_politicas">
    						<li>Se houver o cancelamento do Workshop, o Aluno receberá de volta a totalidade do valor já pago em até 72 horas da comunicação do cancelamento.</li>
    					</ul>
    				</div>
    			</div>
    			<div class="row mt-4">
    				<div class="col-12">
    					<div class="section-title line left text-center text-sm-left mb-3">
							<div class="title">
								<h3 class="mb-0">Ceder a Vaga<span class="d-none d-md-inline"></span></h3>
							</div>
						</div>
					</div>   				
    				<div class="col-12">
    					<ul class="links_politicas">
    						<li>A participação no Workshop é pessoal e intransferível.</li>
    					</ul>
    				</div>
    			</div>
    			<div class="row mt-4">
    				<div class="col-12">
    					<div class="section-title line left text-center text-sm-left mb-3">
							<div class="title">
								<h3 class="mb-0">Condição de Participação<span class="d-none d-md-inline"></span></h3>
							</div>
						</div>
					</div>   				
    				<div class="col-12">
    					<ul class="links_politicas">
    						<li>Somente poderão participar do Workshop os Alunos maiores de 16 anos e que apresentarem registro profissional (DRT) emitido em seu nome. O Aluno maior de 16 e menor de 18 anos deverá apresentar autorização escrita de seu responsável legal. Os Alunos que não apresentarem estes documentos não participarão do Workshop e terão direito à devolução de valores.</li>
    					</ul>
    				</div>
    			</div>    	    			
    		</div>
    	</section>
  <!--================================= header -->		
  		<?php include 'footer.php';?>


	</body>
</html>