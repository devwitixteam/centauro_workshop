<?php 
	
	require_once "PagSeguro.php";
	require_once "Curso.php";

	session_start();

	$usuario = array();

	if(isset($_POST['reserva']))
	{
		preencheInfoUsuario($_POST, 'reserva');
		$_SESSION['tipo'] = 'reserva';
	}

	if(isset($_POST['pagamento']))
	{
		preencheInfoUsuario($_POST, 'pagamento');
		$_SESSION['tipo'] = 'pagamento';
	}

	function preencheInfoUsuario($usuarioinfo, $tipo)
	{
		$data = date('Y-m-d h:i:s');

		$usuario['nome'] = $usuarioinfo['nome'];
		$usuario['telefone'] = $usuarioinfo['telefone'];
		$usuario['celular'] = $usuarioinfo['celular'];
		$usuario['email'] = $usuarioinfo['email'];
		$usuario['cpf'] = $usuarioinfo['cpf'];
		$usuario['drt'] = $usuarioinfo['drt'];
		$usuario['data'] = $data;

		$curso = new Curso();
		$cadastro = $curso->cadastraUsuario($usuario);

		if($cadastro != false)
		{
			realizaCompra($usuario, $cadastro, $tipo);
		}
	}



	function realizaCompra($usuario, $id_usuario, $tipo)
	{
		$pagamento = new PagSeguro();
		$urlAcesso = $pagamento->requisicaoPagamento($usuario,$id_usuario,$tipo);

		if($urlAcesso)
		{
			header('location:'.$urlAcesso);
		}
		else
		{
			return false;
		}
	}


 ?>