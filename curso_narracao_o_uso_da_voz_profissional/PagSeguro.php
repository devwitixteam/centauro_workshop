<?php 	
	header("access-control-allow-origin: https://sandbox.pagseguro.uol.com.br");
?>
<?php 
	
	require_once "../pagseguro/source/PagSeguroLibrary/PagSeguroLibrary.php";

	require_once "Curso.php";

	class PagSeguro
	{

		public function autentica()
		{
			try 
			{
				$credentials = new PagSeguroAccountCredentials($this->email,$this->token);

            	$session = PagSeguroSessionService::getSession($credentials);

            	return true;
			}
			catch (PagSeguroServiceException $e) 
	        {
	            return false;
	        }	
		}

		public function requisicaoPagamento($usuario,$id_usuario, $tipo)
		{
				$paymentRequest = new PagSeguroPaymentRequest();  
				$paymentRequest->addItem('0003', 'Curso - Narração - O uso da voz profissional', 1, 2.00);  
				$paymentRequest->addParameter('senderPhone', 921231232);
    			//$paymentRequest->addParameter('senderBornDate', "11/11/2012");
				$paymentRequest->setCurrency("BRL"); 
				$paymentRequest->setReference($id_usuario);
				$paymentRequest->addPaymentMethodConfig('CREDIT_CARD', 8, 'MAX_INSTALLMENTS_LIMIT');
				//$paymentRequest->excludePaymentMethodGroup('BOLETO', 'BOLETO');
				$paymentRequest->addParameter('tipo', $tipo);

				try 
				{  
  
				  $credentials = PagSeguroConfig::getAccountCredentials();
				  $checkoutUrl = $paymentRequest->register($credentials);

				  $codigo_formulario = substr($checkoutUrl, strpos($checkoutUrl,'code=')+5);

				  $curso = new Curso();
				  $atualizacao_formulario = $curso->atualizaUsuarioFormulario($id_usuario,$codigo_formulario);

				  if($atualizacao_formulario)
				  {
				  	 return $checkoutUrl;
				  }
				  else
				  {
				  	 return false;
				  }
				} 
				catch (PagSeguroServiceException $e) 
				{  
				    return false;
				}
		}

		public function consultaNotificacao($codigoNotificacao)
		{
			    $email = 'marketplace@witix.com.br';
			    $token = '5C63CD1DA616495EB53DD05B334F559E';

			    /*$url = 'https://ws.pagseguro.uol.com.br/v2/transactions/notifications/' . $codigoNotificacao . '?email=' . $email . '&token=' . $token;*/

			    //Caso use sandbox descontente a linha abaixo.
			    $url = 'https://ws.sandbox.pagseguro.uol.com.br/v2/transactions/notifications/' . $codigoNotificacao . '?email=' . $email . '&token=' . $token;

			    $curl = curl_init($url);
			    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
			    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			    $transaction= curl_exec($curl);
			    curl_close($curl);

			    if($transaction == 'Unauthorized')
			    {
			        return false;
			    }
			    else
			    {
			    	$transaction = simplexml_load_string($transaction);
			    	
			    	$curso = new Curso();
			    	$atualizacao = $curso->atualizaUsuarioNotificacao($transaction->code, $transaction->status);

			    	if($atualizacao != false)
			    	{
			    		return true;
			    	}
			    	else
			    	{	
			    		return false;
			    	}
			    }    
		}

		public function consultaTransacao($codigoTransacao)
		{
			$email = 'marketplace@witix.com.br';
		    $token = '5C63CD1DA616495EB53DD05B334F559E';

		    /*$url = 'https://ws.pagseguro.uol.com.br/v2/transactions/notifications/' . $codigoNotificacao . '?email=' . $email . '&token=' . $token;*/

		    //Caso use sandbox descontente a linha abaixo.
		    $url = 'https://ws.sandbox.pagseguro.uol.com.br/v2/transactions/' . $codigoTransacao . '?email=' . $email . '&token=' . $token;

			$_h = curl_init();
			//curl_setopt($_h, CURLOPT_HEADER, 1);

			curl_setopt($_h, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($_h, CURLOPT_HTTPGET, 1);
			curl_setopt($_h, CURLOPT_URL, $url );
			curl_setopt($_h, CURLOPT_SSL_VERIFYPEER, TRUE);
			curl_setopt($_h, CURLOPT_SSL_VERIFYHOST,  2);
			curl_setopt($_h, CURLOPT_DNS_USE_GLOBAL_CACHE, false );
			curl_setopt($_h, CURLOPT_DNS_CACHE_TIMEOUT, 2 );

			$output = curl_exec($_h);

			$transacao = simplexml_load_string($output);

			if($transacao)
			{
				$curso = new Curso();
				$curso->atualizaUsuarioSucesso($transacao->reference,$transacao->code,$transacao->status);
			}
		}
	}

 ?>