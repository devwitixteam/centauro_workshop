
<?php 

	class Curso
	{
		public function verificaVagas()
		{
			$data = '2019-08-03';
			
			$pdo = new PDO('mysql:host=localhost;dbname=cursocentauro','root','');

			$query = $pdo->prepare('select count(id) as vagas_preenchidas from usuarios where status = 3 or data_cadastro <= :data;');

			$query->bindParam(':data',$data);

			$result = $query->execute();

			if($result)
			{
				return $query->fetchAll(PDO::FETCH_ASSOC);
			}
			else
			{
				return false;
			}
		}

		public function buscaAlunos()
		{
			$data = '2019-08-03';
			
			$pdo = new PDO('mysql:host=localhost;dbname=cursocentauro','root','');

			$query = $pdo->prepare('select * from usuarios where status = 3 and data_cadastro <= :data;');

			$query->bindParam(':data',$data);

			$result = $query->execute();

			if($result)
			{
				return $query->fetchAll(PDO::FETCH_ASSOC);
			}
			else
			{
				return false;
			}
		}

		public function atualizaEstadoCurso()
		{

			$data_anterior = date("Y-m-d",strtotime('-2 days', strtotime(date('Y-m-d'))));
			
			$pdo = new PDO('mysql:host=localhost;dbname=cursocentauro','root','');

			$query = $pdo->prepare('update usuarios set status = 0 where data_cadastro < :data;');

			$query->bindParam(':data',$data_anterior);

			$result = $query->execute();

			if($result)
			{
				return $query->fetchAll(PDO::FETCH_ASSOC);
			}
			else
			{
				return false;
			}
		}


		public function cadastraUsuario($usuario)
		{

			$pdo = new PDO('mysql:host=localhost;dbname=cursocentauro','root','');

			$query = $pdo->prepare('insert into usuarios(nome,telefone,celular,email,cpf,drt,data_cadastro) values(:nome,:telefone,:celular,:email,:cpf,:drt,:data_cadastro);');

			$query->bindParam(':nome', $usuario['nome']);
			$query->bindParam(':telefone', $usuario['telefone']);
			$query->bindParam(':celular', $usuario['celular']);
			$query->bindParam(':email', $usuario['email']);
			$query->bindParam(':cpf', $usuario['cpf']);
			$query->bindParam(':drt', $usuario['drt']);
			$query->bindParam(':data_cadastro', $usuario['data']);

			$result = $query->execute();

			if($result)
			{
				return $pdo->lastInsertId();
			}
			else
			{
				return false;
			}
		}

		public function atualizaUsuarioFormulario($usuario_id,$codigo_formulario)
		{

			$pdo = new PDO('mysql:host=localhost;dbname=cursocentauro','root','');

			$query = $pdo->prepare('update usuarios set codigo_formulario= :codigo_formulario where id= :usuario;');

			$query->bindParam(':codigo_formulario', $codigo_formulario);
			$query->bindParam(':usuario', $usuario_id);

			$result = $query->execute();

			if($result)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		public function atualizaUsuarioSucesso($usuario_id,$codigo_transacao, $status)
		{
			$pdo = new PDO('mysql:host=localhost;dbname=cursocentauro','root','');

			$query = $pdo->prepare('update usuarios set codigo_transacao= :codigo_transacao, status= :status where id= :usuario;');

			$query->bindParam(':codigo_transacao', $codigo_transacao);
			$query->bindParam(':status', $status);
			$query->bindParam(':usuario', $usuario_id);

			$result = $query->execute();

			if($result)
			{
				return true;
				
			}
			else
			{
				return false;
			}
		}

		public function atualizaUsuarioNotificacao($codigo_transacao, $status)
		{
			$pdo = new PDO('mysql:host=localhost;dbname=cursocentauro','root','');

			$query = $pdo->prepare('update usuarios set status= :status where codigo_transacao= :codigo_transacao;');

			$query->bindParam(':codigo_transacao', $codigo_transacao);
			$query->bindParam(':status', $status);

			$result = $query->execute();

			if($result)
			{
				return true;	
			}
			else
			{
				return false;
			}
		}
	}
	

 ?>