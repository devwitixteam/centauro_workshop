<!DOCTYPE html>
<html lang="pt-br">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="keywords" content="HTML5 Template" />
<meta name="description" content="Webster - Responsive Multi-purpose HTML5 Template" />
<meta name="author" content="potenzaglobalsolutions.com" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<title>Curso narração - O uso da voz profissional</title>

<!-- Favicon -->
<link rel="shortcut icon" href="../images/favicon.ico" />

<!-- font -->
<link  rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:200,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900">
<link href="https://fonts.googleapis.com/css?family=PT+Serif:400,400i,700,700i" rel="stylesheet">
 
<!-- Plugins -->
<link rel="stylesheet" type="text/css" href="../template/css/plugins-css.css" />

<!-- Typography -->
<link rel="stylesheet" type="text/css" href="../template/css/typography.css" />

<!-- Shortcodes -->
<link rel="stylesheet" type="text/css" href="../template/css/shortcodes/shortcodes.css" />

<!-- Style -->
<link rel="stylesheet" type="text/css" href="../template/css/style.css" />

<!-- Responsive -->
<link rel="stylesheet" type="text/css" href="../template/css/responsive.css" /> 

<!-- Style customizer -->
<link rel="stylesheet" type="text/css" href="../template/css/skins/skin-centauro.css" data-style="styles"/>

<!-- Style customizer -->
<link rel="stylesheet" type="text/css" href="../assets/css/centauro.css" data-style="styles"/>

<style>
	.copyright{min-height: 55px; background: #cf3c48;
      	font-size: 1rem;
    }
    .links_footer{
      	list-style-type:none;
     	font-size: 1rem;
    }

    .bg-overlay-black-60:before {
    	background:rgba(21, 25, 27, 0.8);
    }

    .footer p{
    	font-size: 1rem !important;
    }

   .footer a:hover{
   		color:#cf3c48 !important;
   }

   .btnContrato{
		background-color: #cf3c48;
		transition: all 0.5s;
   }

   .btnContrato:hover{
   		background-color: transparent;
   		border-color: black;
   		color: black !important;
   		transition: all 0.5s;
   }

</style>

</head>

<body>
 
<div class="wrapper" id="home">

  <!--=================================
   preloader -->
    <div id="pre-loader">
      <img src="../template/images/pre-loader/loader-13.svg" alt="">
    </div>
  <!--=================================
   preloader -->

	<!--=================================
	 header -->
		<header id="header" class="header light">
			<div class="menu">  
				<!-- menu start -->
				<nav id="menu" class="mega-menu">
					<!-- menu list items container -->
					<section class="menu-list-items">
						<div class="container"> 
							<div class="row"> 
								<div class="col-lg-12 col-md-12"> 
									<!-- menu logo -->
									<ul class="menu-logo">
										<li>
											<a href="./"><img id="logo_img"  class="img-fluid" src="../assets/img/logo.png" alt=""> </a>
										</li>
									</ul>
									<!-- menu links -->
									<div class="menu-bar">
										<ul class="menu-links">
											<li><a href="./">Home</a></li>
											<li><a href="./#workshop">Sobre Workshop</a></li>
											<li><a href="./#diretores">Convidados</a></li>
											<li><a href="./#centauro">A Centauro</a></li>
											<li><a href="./#duvidas">Dúvidas frequentes</a></li>
											<li><a class="inscrever" href="pagamento.php">Quero me inscrever</a></li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</section>
				</nav>
				<!-- menu end -->
			</div>
		</header>
	<!--=================================
	 header -->

	<!--=================================
	 page-title-->
		<section class="page-title bg-overlay-black-60 parallax" data-jarallax='{"speed": 0.6}' style="background-image: url(../assets/img/bg/bg-5.jpg);">
			<div class="container">
				<div class="row"> 
					<div class="col-lg-12"> 
						<div class="page-title-name">
							<h1 class="m-0">Pagamento</h1>
						</div>
					</div>
				</div> 
			</div>
		</section>
	<!--=================================
	 page-title-->

	<!--=================================
	 page-container-->
		<section class="page-section-ptb">
			<div class="container">
				<div class="row d-flex align-items-center">
					<div class="col-md-7">
						<div class="row pricing-top m-0 px-2">
							<div class="col-12 mb-4">
								<div class="section-title line left text-center text-sm-left mb-3">
									<div class="title">
										<h5 class="mb-0">Sobre o Workshop<span class="d-none d-md-inline">:</span></h5>
									</div>
								</div>
								<div>
									Neste workshop, vamos abordar todo o processo de dublagem, desde técnicas, interpretação, sync, voice over, leitura dinâmica e adaptação textual à legislação, como funciona o mercado de trabalho, o acordo coletivo e, é claro, teremos muita, mas muita prática!								
								</div>
							</div>
							<div class="col-12 mb-4">
								<div class="section-title line left text-center text-sm-left mb-3">
									<div class="title">
										<h5 class="mb-0">Pré-requisitos<span class="d-none d-md-inline">:</span></h5>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12">
										<ul class="list list-unstyled list-hand">
											<li>Possuir DRT</li>
											<li>Ter 18 anos ou mais</li>
										</ul>
									</div>
									<!--<div class="col-sm-6">
										<ul class="list list-unstyled list-hand">
											<li>Lorem ipsum dolor</li>
											<li>Excepteur sint occaecat</li>
										</ul>
									</div>!-->
								</div>
							</div>
							<div class="col-12">
								<div class="row">
									<div class="col-lg-6 mb-4 mb-lg-0">
										<div class="section-title line left text-center text-sm-left mb-3">
											<div class="title">
												<h5 class="mb-0">Datas<span class="d-none d-md-inline">:</span></h5>
											</div>
										</div>
										<div class="box">
											<select class="fancyselect" tabindex="0">
												<option value="0" class="option">Data disponiveis<span class="d-none d-md-inline">:</span></option>
												<option value="1" class="option selected focus">03/08/2019</option>
												<!--<option value="2" class="option disabled" disabled>02/00/2018</option>
												<option value="3" class="option">03/00/2018</option>-->
											</select>
										</div>
									</div>
									<div class="col-lg-6">
										<div class="section-title line left text-center text-sm-left mb-3">
											<div class="title">
												<h5 class="mb-0">Local<span class="d-none d-md-inline">:</span></h5>
											</div>
										</div>
										<div class="">
											Av. Francisco Matarazzo, N° 999 - 10º Andar Barra Funda <br>
											CEP 05001-100 São Paulo, Brasil
										</div>
									</div>
								</div>
							</div>
							<div class="col-12 mb-4">
								<div class="section-title line left text-center text-sm-left mb-3 ">
									<div class="title">
										<h5 class="mb-0">Formas de Pagamento<span class="d-none d-md-inline">:</span></h5>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12">
										<img class="full-width" src="///assets.pagseguro.com.br/ps-integration-assets/banners/pagamento/avista_estatico_550_70.gif" alt="Logotipos de meios de pagamento do PagSeguro" title="Este site aceita pagamentos com as principais bandeiras e bancos, saldo em conta PagSeguro e boleto.">
									</div>
									<!--<div class="col-sm-6">
										<ul class="list list-unstyled list-hand">
											<li>Lorem ipsum dolor</li>
											<li>Excepteur sint occaecat</li>
										</ul>
									</div>!-->
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-5">
						<div class="contact-form clearfix">
							<form id="pagamento" action="solicita_pagamento.php" method="post">
							<!-- <form id="pagamento" name="pagamento" method="post" action="solicita_pagamento.php"> -->
								<div class="row">
									<div class="col-12 py-3 text-center">
										<h4>CADASTRO</h4>
									</div>
									<div class="col-12">
										<div class="section-field w-100">
											<input type="text" placeholder="Nome*" class="form-control"  name="nome" required="required">
										</div>
									</div>
									<div class="col-12">
										<div class="section-field w-100">
											<input type="text" placeholder="Telefone" class="form-control" id="telefone" name="telefone">
										</div>
									</div>
									<div class="col-12">
										<div class="section-field w-100">
											<input type="text" placeholder="Celular*" class="form-control" id="celular" name="celular" required="required">
										</div>
									</div>
									<div class="col-12">
										<div class="section-field w-100">
											<input type="email" placeholder="Email*" class="form-control" name="email" required="required">
										</div>
									</div>
									<div class="col-12">
										<div class="section-field w-100">
											<input type="text" id="cpf" placeholder="CPF*" class="form-control" name="cpf" required="required">
										</div>
									</div>
									<div class="col-12">
										<div class="section-field w-100">
											<input type="text" id="drt" placeholder="DRT*" class="form-control" name="drt" required="required">
										</div>
									</div>
									<div class="col-12">
										<div class="section-field w-100 ml-1">
											<input type="checkbox" id="politicas" name="politica" required="required">
  											<label for="politicas">Eu li, e aceito os termos da política de uso. <a href="../politicas.php" target="_blank" data-toggle="modal" data-target="#contratoModal">Acessar contrato.</a></label>
										</div>
									</div>
								</div>
								<?php
									require_once "../verificavagas.php";
								?>
								<!-- <div class="section-field textarea clearfix text-center pt-4">
									<button class="button dark border w-100 px-2">
										<span>EFETUAR PAGAMENTO</span> <i class="fa fa-money"></i>
									</button>
									<button class="button dark border w-100 m-0">
										<span>FAZER RESERVA</span> <i class="fa fa-calendar-check-o"></i>
									</button>
								</div> -->
							</form>
						</div>
					</div>
					<div class="col-12">
						<div class="section-field w-100 clearfix mt-4">
							<div class="alert alert-primary">
								É obrigatório ter a DRT para comprar o curso.
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- MODAL CONTRATO -->
		<div class="modal fade" id="contratoModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  			<div class="modal-dialog" role="document">
    			<div class="modal-content">
			      <div class="modal-header d-flex justify-content-center text-center">
			        <h5 class="modal-title" id="ModalLabel">Contrato da Política e Termos de Uso</h5>
			      </div>
			      <div class="modal-body">
			        <span>
			        	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum mauris felis, ullamcorper eget nunc quis, iaculis iaculis nisi. Proin dignissim euismod dui. Phasellus non velit in nisi accumsan cursus. In nec tincidunt dui. Etiam at ipsum et erat rutrum vulputate ac eu velit. Vestibulum aliquet dolor quis feugiat tristique. Nunc sed massa sit amet metus elementum maximus. Proin dictum finibus cursus. Suspendisse nec dictum ligula, sit amet euismod turpis. Cras venenatis nec nulla in aliquet. Donec condimentum urna eget justo efficitur, volutpat auctor diam cursus. Mauris at lacus pulvinar, pharetra libero vel, molestie augue. In euismod orci id tristique tempor. Donec vel interdum massa. Proin turpis sem, tincidunt quis libero sit amet, pellentesque interdum turpis. Sed et pulvinar massa.
			        </span>
			      </div>
			      <div class="modal-footer">
			        <button type="button" class="btn btnContrato w-100 text-white" data-dismiss="modal">Fechar</button>
			      </div>
			   	</div>
			</div>
		</div>
	<!--=================================
	 page-container-->

<?php
	include 'footer.php';
?>
<script type="text/javascript" src="../template/js/jquery-nice-select/jquery-nice-select.js"></script>

<!-- <script type="text/javascript" src="../https://stc.pagseguro.uol.com.br/pagseguro/api/v2/checkout/pagseguro.lightbox.js"></script>
<script>
	function enviaPagseguro(){
		$.post('solicita_pagamento.php','',function(data)
		{
			alert('teste');
			$('#code').val(data);
			$('#pagamento').submit();
		})
	}
</script> -->